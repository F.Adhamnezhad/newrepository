import React, { Fragment, useState } from 'react';
import './App.css';
import Routes from '../Router/Routes';
import { CartProvider } from '../Components/ShoppingCart/CartContext';

const App = () => {
  const [cart, setCart] = useState([]);
  const URL = 'http://api.jora.market';
  return (
    <CartProvider>
      <div style={{ position:"relative" }}>
        <Routes url={URL} />
      </div>
    </CartProvider>

  );
}

export default App;

