import React, { Fragment } from 'react';
import RoutHeader from '../Common/RouteHeader';
import NOtFoundCss from './NotFound1.module.css';
import pic from '../../Assets/img/NotFound/image 1.svg';
import { Link } from 'react-router-dom';

const NotFound = () => {
    return (
        <Fragment>
            <div className={`container-fluid ${NOtFoundCss.div}`}>
            <div className="container">
                <div className={`col-12 ${NOtFoundCss.head}`}>
                    <RoutHeader pageTitle="404" />
                </div>
            </div>
            </div>
            <div className={NOtFoundCss.divmain}>
                <figure>
                    <img src={pic} alt='...' />
                </figure>
                <h4 className={NOtFoundCss.h4}> (; صفحه ای که دنبالش بودی پیدا نکردم</h4>
                <button className={NOtFoundCss.btn}> <Link className={NOtFoundCss.Link} to="/">برو به خانه</Link> </button>
            </div>

        </Fragment>
    );
}

export default NotFound;


