import React, { Fragment } from 'react';
import InputCss from './Input.module.css';

const Input = (props) => {
 
    return (

        <div className={`col-12 col-md-6 ${InputCss.main}`}>
            <span>{props.title}</span>
            <input
             type={props.type}  
             placeholder={props.placeholder}
            />
        </div>

    );
}

export default Input;
