import React, { Fragment } from 'react';
import { ErrorMessage, useField } from 'formik';

const Input = ({className , ...props }) => {
    const [field, meta] = useField(props);
    return (
        <input
            {...field} {...props}
            autoComplete="off"
            className={className}
        />
    );
}

export default Input;
