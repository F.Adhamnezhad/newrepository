import React, { Fragment } from 'react';
import RouteHeaderCss from './RouteHeader.module.css';
import { Link } from 'react-router-dom';

const RoutHeader = (props) => {
    return (
        <Fragment>
            <header>
                <nav className={`container ${RouteHeaderCss.header}`}>
                    <div className={`row`}>
                    
                            <ul >
                            <li><Link className={RouteHeaderCss.Link} to="/">خانه</Link></li>
                                <li>{props.pageTitle}</li>
                            </ul>
                        </div>
                </nav>
            </header>
        </Fragment>
    );
}

export default RoutHeader;
