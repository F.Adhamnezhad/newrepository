import React from 'react';
import Sh_B_Css from './Shopping_btn.module.css'
const Shopping_btn = (props) => {
    const color= props.color;
    return (
        <div>
            <button type={props.btntype} style={{background: color}} onClick={props.handleState} className={Sh_B_Css.btn}>{props.btn_ti}</button>
        </div>
    );
}

export default Shopping_btn;
