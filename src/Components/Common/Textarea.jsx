import React, { Fragment } from 'react';
import InputCss from './Input.module.css';
import { ErrorMessage, useField } from 'formik';
import cx from 'classnames';

const Textarea = ({ title, ...props }) => {
    const [field, meta] = useField(props);
    return (
        <Fragment>
            <div className={`col-12 ${InputCss.main}`}>
                <span>{title}</span>
                <textarea
                    {...field} {...props}
                    autoComplete="off"
                    className={meta.touched && meta.error ? cx(InputCss.inputErr, InputCss.input) : InputCss.input}
                />
                <div>
                    <ErrorMessage component="div" className={InputCss.err} name={field.name} />
                </div>
            </div>
        </Fragment>
    );
}

export default Textarea;



