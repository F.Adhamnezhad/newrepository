import React, { Fragment } from 'react';
import InputCss from './CustomInput.module.css';
import { ErrorMessage, useField} from 'formik';
import cx from 'classnames';

const Input = ({ title, spanCss, ...props }) => {
    const [field, meta] = useField(props);
    return (
        <div className={`col-md-12 col-lg-10 ${InputCss.main}`} >
            <span className={spanCss}>{title}</span>
            <input
              {...field} {...props}
              autoComplete="off"
              className={meta.touched && meta.error ? cx(InputCss.inputErr , InputCss.input) : InputCss.input } 
            />
            <div>
                <ErrorMessage component="div" className={InputCss.err}  name={field.name} />
            </div>
        </div>
    );
}

export default Input;
