import React, { Fragment } from 'react';
import TabCss from './ContactTab.module.css';
import About from './Content/About';
import FAQ from './Content/FAQ/FAQ';
import FormContact from './Content/FormContact';
import MainTab from './MainTab';
import ContactTabNav from './Navbar/ContactTabNav';


const tabs = [
    {
        id: 1,
        name: "تماس با ما",
        content: <FormContact/>
    },
    {
        id: 2,
        name: "درباره جورا",
        content: <About/>
    },
    {
        id: 3,
        name: "سوالات متداول",
        content: <FAQ/>
    }
];

const ContactTab = (props) => {

    return (
        <Fragment>
            <div className={`container-fluid ${TabCss.main}`}>
                <div className={TabCss.tab}>
                <MainTab selectId={props.selectId}  tabs={tabs}/>
                </div>
            </div>
        </Fragment>
    );
}

export default ContactTab;
