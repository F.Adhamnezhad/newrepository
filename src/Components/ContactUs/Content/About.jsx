import React, { Fragment } from 'react';
import JoraImg from '../../../Assets/img/ContactUs/pic.svg';
import ImgF from '../../../Assets/img/ContactUs/F.svg';
import ImgD from '../../../Assets/img/ContactUs/D.svg';
import ImgT from "../../../Assets/img/ContactUs/T.svg";
import AboutCss from './About.module.css'
import cx from 'classnames'
 
    
const text= "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد."
const Team =[
    {id:1 ,img: ImgD , name: "ماکان دوستدار" , post:"مدیر"},
    {id:2 ,img: ImgT , name: "سید مصطفی طباطبایی" , post:"مدیر و موسس"},
    {id:3 ,img: ImgF , name: "فائزه ادهم نژاد" , post:"فرانت اند دولوپر"}
]    
const About = () => {
    return (
        <Fragment>

            <div className={`col-12 ${AboutCss.about}`}>
                <div className={`col-12  col-md-4 ${AboutCss.about_img}`}>
                    <figure>
                        <img src={JoraImg} alt="..." />
                    </figure>
                </div>

                <div className={`col-12 col-md-8 ${AboutCss.about_text}`}>
                    <p className={AboutCss.ti}>درباره جورا مارکت</p>
                    <span>{text}</span>
                </div>
            </div>
            <div className={AboutCss.team_main}>
            <p className={cx(AboutCss.title, AboutCss.ti)}>تیم جورا مارکت</p>
            <div className={`col-12 ${cx(AboutCss.about_team)}`}>
            
            {Team.map(t=>(
                <div className={AboutCss.teamBio} key={t.id}>
                    
                     <figure>
                        <img src={t.img} alt="..." />
                    </figure>
                    <span>{t.name}</span>
                    <span className={AboutCss.teamBio_info}>{t.post}</span>     
                </div>
            ))}
            </div>
            <button className={AboutCss.join}>به تیم ما بپیوندید</button>
            </div>
        </Fragment>
    );
}

export default About;
