import React, { useState, Fragment } from 'react';
import AccordionCss from './Accordion_Component.module.css';
import { Card, Accordion } from 'react-bootstrap'
import cx from 'classnames';



const Accordion_Component = (props) => {

    const [Items, setItems] = useState(props.data);

    return (
        <Fragment>
            <Accordion defaultActiveKey={Items[0].id+1}  className={AccordionCss.accordion}>
                {Items.map(i => (
                    <Card key={i.id} className={AccordionCss.card}>
                        <Accordion.Toggle className={AccordionCss.header} as={Card.Header} eventKey={i.id + 1}>
                            {i.header}
                        </Accordion.Toggle>
                        <Accordion.Collapse className={AccordionCss.collapse} eventKey={i.id + 1}>
                            <Card.Body className={AccordionCss.body} >{i.body}</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                ))}
            </Accordion>

        </Fragment>
    );

}

export default Accordion_Component;
