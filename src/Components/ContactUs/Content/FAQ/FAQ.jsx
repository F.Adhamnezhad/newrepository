import React, { Fragment } from 'react';
import FAQCss from './FAQ.module.css';
import Accordion_Component from './Accordion_Component';
import FaqQuestions from './FaqQuestions';
import {Link} from 'react-router-dom';

const data = [
    { id: 0, header: "چند روز طول میکشه به دستم برسه؟", body: "پاسخ سوالات", open: true },
    { id: 1, header: "چند روز طول میکشه به دستم برسه؟", body: "پاسخ سوالات", open: true },
    { id: 2, header: "چند روز طول میکشه به دستم برسه؟", body: "پاسخ سوالات", open: false },
    { id: 3, header: "چند روز طول میکشه به دستم برسه؟", body: "پاسخ سوالات", open: false },
    { id: 4, header: "چند روز طول میکشه به دستم برسه؟", body: "پاسخ سوالات", open: false }
];

const FAQ = () => {
    return (
        <Fragment>
            <div className={`col-12 ${FAQCss.main}`}>
                <div className={`col-md-3  col-12 ${FAQCss.right}`}>
                    <div className={FAQCss.right_sec1}>
                    </div>
                    <div className={FAQCss.site} >
                        <p> بخشی از سوالات پرتکرار</p>
                        <p>شما کاربران در جورامارکت</p>
                        <div className={FAQCss.t}>پاسخ خود را پیدا نکردید؟</div>
                    </div>
                    <div className={FAQCss.mob}>
                        بخشی از سوالات پرتکرار کاربران جورا مارکت
                    </div>
                    <Link to="/contactus"> با ما تماس بگیرید </Link>
                </div>
                <div className={`col-md-9 col-12 ${FAQCss.left}`}>
                <FaqQuestions data={data} />
                </div>
            </div>
        </Fragment>
    );
}

export default FAQ;
