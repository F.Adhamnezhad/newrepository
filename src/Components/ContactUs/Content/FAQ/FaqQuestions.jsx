import React, { Fragment } from 'react';
import Accordion_Component from './Accordion_Component';
import FAQCss from './FAQ.module.css'
const FaqQuestions = (props) => {
    return (
        <Fragment>
            <Accordion_Component data={props.data} />
        </Fragment>
    );
}

export default FaqQuestions;
