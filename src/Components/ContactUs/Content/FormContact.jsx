import React, { Fragment, useState } from 'react';
import FormCss from './FormContact.module.css'
import cx from 'classnames';
import Input from '../../Common/CustomInput'
import Textarea from '../../Common/Textarea';
import { Formik, Form } from 'formik';
import ValidateContactForm from '../../../Core/Validations/ContactForm';

const FormContact = (props) => {
    const [FormData, setFormData] = useState(
        {
            userName: '',
            phoneNumber: '',
            email: '',
            subtitle: '',
            message: ''
        });

    const onSubmit = (values) => {
        setFormData(values);
    }
    console.log("FormData2", FormData);
    return (
        <Fragment>
            <div className={`col-12 ${FormCss.main}`}>
                <div className={`col-12 col-md-2 ${FormCss.right}`}>

                    <div className={FormCss.card}>
                        <div className={FormCss.card_img}> </div>
                        <span >تماس:</span>
                        <span>09112345678</span>
                    </div>
                    <div className={FormCss.card}>
                        <div className={cx(FormCss.card_img2, FormCss.card_img)}> </div>
                        <span >ایمیل:</span>
                        <span>info@jora.market</span>
                    </div>
                    <div className={FormCss.card}>
                        <div className={cx(FormCss.card_img3, FormCss.card_img)}> </div>
                        <span >آدرس:</span>
                        <span>بازکردن در نقشه</span>
                    </div>
                </div>

                <Formik
                    initialValues={FormData}
                    validationSchema={ValidateContactForm}
                    onSubmit={(newData) => onSubmit(newData)}
                    enableReinitialize="true"
                >
                    {() => (

                        <div className={`col-md-10 ${FormCss.left}`}>
                            <span className={`col-12 ${FormCss.left_span}`}>برای ارتباط با جورا مارکت همچنین می توانید از طریق فرم زیر نیز با ما در تماس باشید.</span>
                            <Form className={FormCss.form}>
                                <div className={`col-12 ${FormCss.sec1}`}>
                                    <Input
                                        title="نام و نام خانوادگی"
                                        type="text"
                                        name="userName"
                                        placeholder="نام و نام خانوادگی خود را وارد نمایید"
                                    />
                                    <Input
                                        title="شماره همراه"
                                        type="text"
                                        name="phoneNumber"
                                        placeholder="شماره همراه خود را وارد نمایید"
                                    />
                                </div>
                                <div className={`col-12 ${FormCss.sec1}`}>
                                    <Input
                                        title="ایمیل"
                                        type="text"
                                        name="email"
                                        placeholder="ایمیل خود را وارد نمایید"
                                    />
                                    <Input
                                        title="موضوع"
                                        type="text"
                                        name="subtitle"
                                        placeholder="موضوع پیغام خود را وارد نمایید " />
                                </div>
                                <div className={`col-12 ${FormCss.sec1}`}>
                                    <Textarea
                                        title="متن پیام"
                                        name="message"
                                        placeholder="پیغام خود را وارد نمایید" />
                                </div>
                                <button type="submit">ارسال پیام</button>
                            </Form>
                        </div>

                    )}
                </Formik>
            </div>
        </Fragment>
    );
}

export default FormContact;
