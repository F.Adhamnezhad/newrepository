import React, { Fragment } from 'react';

const TabContent = (props) => {
    return (
        <Fragment>
            <div> {props.tab.content} </div>
        </Fragment>
    );
}

export default TabContent;
