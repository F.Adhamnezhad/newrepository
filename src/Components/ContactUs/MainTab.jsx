import React, { Fragment } from 'react';
import ContactTabNav from './Navbar/ContactTabNav';
import TabContent from './Content/TabContent';
import ContentCss from "./ContactTab.module.css"
const MainTab = (props) => {

    const [activeTabId, setActiveTab] = React.useState(props.selectId);

    const activeTab = React.useMemo(() => (
        props.tabs.find((tab) => (
            tab.id === activeTabId
        ))
    ), [activeTabId, props.tabs]);

    return (
        <Fragment>
            <ContactTabNav tabs={props.tabs} onNavClick={setActiveTab}
                activeTabId={activeTabId} />
            <div className={`col-12 col-lg-8 ${ContentCss.information}`}>
                <TabContent tab={activeTab} />
            </div>
        </Fragment>
    );
}

export default MainTab;
