import React, { Fragment } from 'react';
import ContactTabNavCss from './ContactTabNav.module.css';
import cx from 'classnames';
import { Link } from 'react-router-dom';
const ContactTabNav = (props) => {
    return (
        <Fragment>
            
            <div  className={ContactTabNavCss.con}>
            <ul className={`col-9 col-md-7 ${ContactTabNavCss.tabs__nav}`}>
              
                {props.tabs.map((item) => (
                    <li key={item.id}>
                        {(props.activeTabId === item.id) ?
                            <button onClick={() => props.onNavClick(item.id)} className={cx(ContactTabNavCss.tabs__button)}><Link className={ContactTabNavCss.Link} to= { item.id ===1 && "/contactus/contact" || item.id === 2 && "/contactus/about" || item.id === 3 && "/contactus/faq" }>{item.name}</Link></button>
                            :
                            <button onClick={() => props.onNavClick(item.id)} className={ContactTabNavCss.tabs__item}> <Link className={ContactTabNavCss.Link} to= { item.id ===1 && "/contactus/contact" || item.id === 2 && "/contactus/about" || item.id === 3 && "/contactus/faq" }>{item.name}</Link> </button>
                        }
                    </li>
                ))}
               
            </ul>
            </div>
        </Fragment>
    );
}

export default ContactTabNav;
