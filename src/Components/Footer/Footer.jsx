import React, { Fragment } from 'react';
import FooterCss from "./Footer.module.css";
import Namad from "../../Assets/img/Footer/namad.svg";
import Tel from "../../Assets/img/Footer/tel.svg";
import Insta from "../../Assets/img/Footer/insta.svg";
import Tw from "../../Assets/img/Footer/twe.svg";
import Ut from "../../Assets/img/Footer/ut.svg";
import cx from 'classnames';
import ScrollToTop from './ScrollToTop';
import {Link} from 'react-router-dom';

const Footer = () => {
    return (
        <Fragment>
            <div className={`container-fluid ${FooterCss.footer}`}>
            <ScrollToTop showBelow={250}/>
                <footer className="container">
                    <div className={`col-sm-12 ${FooterCss.mainfooter}`}>
                        <div className={`col-6 col-md-4 col-lg-2  ${FooterCss.sec1}`}>
                            <ul className={FooterCss.ul__sec1}>
                                <li className={FooterCss.title}>دسترسی سریع</li>
                                <li><Link className={FooterCss.ul__a} to="/trackorders">پیگیری سفارشات</Link></li>
                                <li><Link className={FooterCss.ul__a} to="/contactus/about">درباره ما</Link></li>
                                <li><Link className={FooterCss.ul__a} to="/contactus/contact">تماس با ما</Link></li>
                            </ul>
                        </div>
                        <div className={`col-3 col-lg-2 col-md-4 ${FooterCss.sec2}`}>
                            <ul className={FooterCss.ul__sec1}>
                                <li className={FooterCss.title}>دسترسی سریع</li>
                                <li><Link className={FooterCss.ul__a} to="/trackorders">پیگیری سفارشات</Link></li>
                                <li><Link className={FooterCss.ul__a} to="/contactus/about">درباره ما</Link></li>
                                <li><Link className={FooterCss.ul__a} to="/contactus/contact">تماس با ما</Link></li>
                            </ul>
                        </div>
                        <div className={`col-3 col-md-2 ${cx(FooterCss.sec3 ,FooterCss.application )}`}>
                            <ul className={FooterCss.ul__sec1}>
                                <li className={cx(FooterCss.title , FooterCss.ti)}>دانلود اپلیکیشن</li>
                            </ul>
                        </div>
                        <div className={`col-5 col-md-4 ${FooterCss.sec4}`}>
                            <ul className={FooterCss.ul__sec1}>
                                <li className={FooterCss.title}>خبرنامه</li>
                            </ul>
                            <div className={FooterCss.khabar}>
                                <span>آدرس ایمیل</span>
                                <input type="email"></input>
                                <button>ثبت</button>
                            </div>
                        </div>
                        <div className={` col-6 col-md-4 col-lg-2  ${FooterCss.sec5}`}>
                            <ul className={FooterCss.ul__sec1}>
                                <li className={FooterCss.title}>نمادها</li>
                            </ul>
                            <ul className={FooterCss.ul__namad}>
                                <li>
                                    <figure><Link to="/"><img src={Namad} alt="namad"/></Link></figure>
                                </li>
                                <li>
                                    <figure><Link to="/"><img src={Namad} alt="namad"/></Link></figure>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div className={`col-md-12 ${FooterCss.footer2}`}>
                        <ul className={FooterCss.ul__sotial}>
                            <li>
                                <figure>
                                    <img src={Tel} alt="telegram"/>
                                </figure>
                            </li>

                            <li>
                                <figure>
                                    <img src={Ut} alt="youtube"/>
                                </figure>
                            </li>

                            <li>
                                <figure>
                                    <img src={Insta} alt="instagram"/>
                                </figure>
                            </li>

                            <li>
                                <figure>
                                    <img src={Tw} alt="twitter"/>
                                </figure>
                            </li>
                        </ul>
                    </div>

                    <div className={`col-md-12 ${FooterCss.footer3}`}>
                    کلیه حقوق متعلق به وبسایت جورا مارکت می باشد و هرگونه کپی، پیگرد قانونی دارد.
                    </div>

                </footer>
            </div>

        </Fragment>
    );
}

export default Footer;
