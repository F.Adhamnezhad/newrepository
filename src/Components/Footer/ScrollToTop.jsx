import React, { Fragment, useState, useEffect } from 'react';
import ScrollCss from "./ScrollToTop.module.css";


const ScrollToTop = ({showBelow}) => {

    const [show, setShow] = useState(showBelow ? false : true);
    const handleScroll = () => {
        if (window.pageYOffset > showBelow) {
            if (!show) { setShow(true) }
            else
                if (show) { setShow(false) }

        }
    }

    useEffect(() => {
        if (showBelow) {
            window.addEventListener(`scroll`, handleScroll);
            return () => window.removeEventListener(`scroll`, handleScroll);
        }
    });



    const handleClick=()=>{
        window[`scrollTo`]({top:0 , behavior:"smooth"})
    }

    return (

        <Fragment>
            <div className={ScrollCss.scroll} onClick={handleClick}>

            </div>
        </Fragment>
    );
}

export default ScrollToTop;
