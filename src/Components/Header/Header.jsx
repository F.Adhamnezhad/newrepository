import React, { Fragment, useState, useContext } from 'react';
import HeaderCss from '../Header/Header.module.css';
import Logo1 from "../../Assets/img/Header/Logo1.svg";
import Logo from "../../Assets/img/Header/Logo.svg";
import Cartlogo from "../../Assets/img/Header/Cart.svg";
import HamburgerMenu from "../Header/Nav/HamburgerMenu";
import MobileCart from "../../Assets/img/Header/mobile_Cart.svg";
import { Link } from 'react-router-dom';
import cx from 'classnames';
import { CartContext } from '../ShoppingCart/CartContext';

const Header = (props) => {
    const [cart, setCart] = useContext(CartContext);
    return (
        <Fragment>
            <header>
                <nav className={`col-12 ${HeaderCss.navbar}`}>

                    <ul className={`col-9 ${HeaderCss.menu}`}>
                        <li className={HeaderCss.logo}>
                            <Link to="/">
                                <figure className={HeaderCss.logofig}>
                                    <img className={HeaderCss.logo_pic} src={Logo1} alt="Logo1" />
                                    <img src={Logo} alt="Logo" />
                                </figure>
                            </Link>
                        </li>
                        <li className={HeaderCss.menu_li}><Link className={HeaderCss.a_menu} to="/">صفحه اصلی</Link></li>
                        <li className={HeaderCss.menu_li}><Link className={HeaderCss.a_menu} to="/products">فروشگاه</Link></li>
                        <li className={HeaderCss.menu_li}><Link className={HeaderCss.a_menu} to="/trackorders">پیگیری سفارشات</Link></li>
                    </ul>
                    <figure className={`col-1 ${HeaderCss.shoppingcart_logo}`}>
                        <Link to="/shoppingcart">
                            <span className={HeaderCss.totalCart}>
                                {cart.length}
                            </span>
                        </Link>
                        <Link to="/shoppingcart"><img className={HeaderCss.cart_pic} src={Cartlogo} alt="cart" /></Link>
                    </figure>
                </nav>

                {/* ....................... Mobile__Menu ............................................... 
                     ........................................................................................*/}


                <nav className={`col-12 ${HeaderCss.mobile___nav}`}>

                    <div className={`col-12 ${HeaderCss.menu_mobile}`}>

                        <div className={`col-4 ${HeaderCss.menu_mobile_menu}`}>
                            <HamburgerMenu />
                        </div>
                        <Link className="col-4" to="/">
                            <div className={` ${HeaderCss.menu_mobile_logo}`}>

                            </div>
                        </Link>

                        <div className={`col-4 ${HeaderCss.menu_mobile_cart}`}>
                            <figure className={HeaderCss.img_menu}>
                                <Link to="/shoppingcart"><img src={MobileCart} alt="cart" /></Link>
                                <Link to="/shoppingcart">
                                    <span className={HeaderCss.totalCart}>
                                        {cart.length}
                                    </span>
                                </Link>
                            </figure>
                        </div>

                    </div>


                    <div className={`col-12  ${HeaderCss.menu_mobile_2}`}>
                        <div className={`col-4  ${cx(HeaderCss.menu_mobile_2)}`}>
                            <a className={HeaderCss.menu_mobile_2_submenu} href="#">تماس با ما</a>
                        </div>
                        <div className={`col-4  ${cx(HeaderCss.menu_mobile_2)}`}>
                            <a className={HeaderCss.menu_mobile_2_submenu} href="#">درباره جورا</a>
                        </div>
                        <div className={`col-4  ${cx(HeaderCss.menu_mobile_2)}`}>
                            <a className={HeaderCss.menu_mobile_2_submenu} href="#">سوالات متداول</a>
                        </div>
                    </div>

                </nav>





            </header>
        </Fragment >
    );
}

export default Header;
