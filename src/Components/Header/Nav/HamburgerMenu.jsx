import React, { Fragment, useState} from 'react';
import RightNav from './RightNav';
import styles from "styled-components";

const Styledburger = styles.div `

    width:30px;
    height:30px;
    position: absolute;
    right:16px;
    display: flex;
    justify-content: space-around;
    flex-flow:column nowrap;
    direction: rtl;
    cursor: pointer;
    z-index:1200;
    

    div{
        height: 2px;
        background-color: ${({open}) => open? "#f39237" : "#fff"};
        border-radius: 2px;
        width:30px;
        opacity:90%;
        transform-origin: 1px;
        transition: all .3s linear;
    }

    div:nth-child(1){
        transform: ${({open})=> open? "rotate(45deg)" : "rotate(0deg)"}
    }
    div:nth-child(2){
        transform: ${({open})=> open? "translateX(100%)" : "translateX(0%)"};
        opacity: ${({open})=> open? 0 : 1};
    }
    div:nth-child(3){
        transform: ${({open})=> open? "rotate(-45deg)" : "rotate(0deg)"}
    }
`;

const HamburgerMenu = () => {
     const [open, setOpen] = useState(false);
    return (
        <Fragment>
            <nav>
                <Styledburger  open={open} onClick={()=> setOpen(!open)} >
                    <div></div>
                    <div></div>
                    <div></div>
                </Styledburger>
               <RightNav setOpen={setOpen} open={open}/>
            </nav>
        </Fragment>
    );
}

export default HamburgerMenu;
