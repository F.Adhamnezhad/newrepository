import React, { useState } from 'react';
import styles from "styled-components";
import RnavCss from "./RightNav.module.css";
import cx from 'classnames';
import { Link } from 'react-router-dom';
import ut from '../../../Assets/img/Header/Nav/ut.svg';
import tw from '../../../Assets/img/Header/Nav/Group.svg'
import ins from '../../../Assets/img/Header/Nav/ins.svg';
import tel from '../../../Assets/img/Header/Nav/tel.png';

const StyleNav = styles.ul`
    displayL: flex;
    height: 610px;
    padding-top: 50px;
    padding-bottom:50px;
    padding-right:0;
    padding-left:0;
    flex-direction: column;
    list-style: none;
    background: #fff;
    width: 300px;
    border-radius: 12px 0px 0px 12px;
    position: fixed;
    top: 0;
    right: 0;
    transform: ${({ open }) => open ? "translateX(0%)" : "translateX(100%)"};
    transition: transform .3s ease-in-out ;
    z-index:1000;
`;


const RightNav = ({ open, setOpen }) => {

    return (
        <StyleNav open={open}>
            <Link to="/">
            <div className={RnavCss.nav__img}> </div>
            </Link>
            <Link onClick={() => { setOpen(!open) }} className={RnavCss.li__a} to="/">
                <li className={RnavCss.navli}>
                    <span className={RnavCss.li__span1}>خانه</span>
                </li>
            </Link>
            <Link onClick={() => { setOpen(!open) }} className={RnavCss.li__a} to="/trackorders">
                <li className={RnavCss.navli}>
                    <span className={cx(RnavCss.li__span1, RnavCss.li__span2)}>پیگیری سفارشات</span>
                </li>
            </Link>
            <Link className={RnavCss.li__a} to="/products">
                <li onClick={() => { setOpen(!open) }} className={RnavCss.navli}>
                    <span className={cx(RnavCss.li__span1, RnavCss.li__span3)}>فروشگاه</span>
                </li>
            </Link>
            <Link onClick={() => { setOpen(!open) }} className={RnavCss.li__a} to="/contactus/contact">
                <li className={RnavCss.navli}>
                    <span className={cx(RnavCss.li__span1, RnavCss.li__span4)}>تماس با ما</span>
                </li>
            </Link>
            <Link onClick={() => { setOpen(!open) }} className={RnavCss.li__a} to="/contactus/about">
                <li className={RnavCss.navli}>
                    <span  className={cx(RnavCss.li__span1, RnavCss.li__span5)}>درباره ما</span>
                </li>
            </Link>
            <div className={RnavCss.div_sotial}>
                <figure>
                    <img src={tw}/>
                </figure>
                <figure>
                    <img src={ins}/>
                </figure>
                <figure>
                    <img src={ut}/>
                </figure>
                <figure>
                    <img src={tel}/>
                </figure>
            </div>
        </StyleNav>

    );
}

export default RightNav;
