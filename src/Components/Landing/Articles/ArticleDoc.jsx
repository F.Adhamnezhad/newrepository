import React, { Fragment } from 'react';
import ArticlesCss from './Articles.module.css';
import Aimg from '../../../Assets/img/Landing/Articles/Aimg.svg';
import cx from 'classnames';
const ArticleDoc = () => {
    return (
        <Fragment>
            <div className={`${cx(ArticlesCss.sec1 , ArticlesCss.hide)}`}>
                <div className={ArticlesCss.pic}>
                    <figure>
                        <img src={Aimg} alt="img" />
                    </figure>
                </div>
                <div className={ArticlesCss.text}>
                    <p className={ArticlesCss.title}> صنایع دستی چی بخریم؟</p>
                    <p className={ArticlesCss.info}>صنایع دستی یا کار دستی نوعی کار است که در آن لوازم تزئینی و کاربردی تنها با استفاده از دستیا ابزار ساده ساخنه می شود.معمولا این کلمه به روش های سنتی ساختن کالاها و وسایل مختلف اطلاق می گردد.</p>
                </div>
            </div>
        </Fragment>
    );
}

export default ArticleDoc;
