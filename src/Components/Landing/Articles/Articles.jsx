import React, { Fragment } from 'react';
import ArticlesCss from './Articles.module.css';
import ArticleDoc from './ArticleDoc';
import FirstArt from './FirstArt';

const Articles = () => {
    return (
        <Fragment>
            <div className="container">
                <div className={`col-md-12 ${ArticlesCss.article}`}>

                    <FirstArt />
                    <ArticleDoc />
                    <ArticleDoc />

                </div>
            </div>
        </Fragment>
    );
}

export default Articles;
