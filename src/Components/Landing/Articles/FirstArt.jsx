import React, { Fragment, useState } from 'react';
import ArticlesCss from './Articles.module.css';
import cx from 'classnames';
import Jora1 from '../../../Assets/img/Landing/Articles/Jora 1.svg';

const FirstArt = () => {
    const [show1, setShow1] = useState(1);
    const [show2, setShow2] = useState(0);
    const [hideImg, sethideImg] = useState(1);
    const handleClick = () => {
        setShow1(0);
        setShow2(1);
        sethideImg(0)
    }
    return (
        <Fragment>
            <div className={`${cx(ArticlesCss.sec1, ArticlesCss.sec__jora)}`}>
                <div className={cx(ArticlesCss.pic, ArticlesCss.pic_jora, ArticlesCss.pc__mob)}>
                    <figure>
                        <img src={Jora1} alt="img" />
                    </figure>
                </div>
                {hideImg === 1 &&
                    <div className={cx(ArticlesCss.pic, ArticlesCss.pic_jora, ArticlesCss.pic__pc)}>
                        <figure>
                            <img src={Jora1} alt="img" />
                        </figure>
                    </div>
                }
                <div className={ show1 === 1 ? ArticlesCss.text : cx(ArticlesCss.text,ArticlesCss.txt2 ) }>
                    <p className={ArticlesCss.title}> چرا جورا مارکت؟</p>
                    {show1 === 1 &&
                        <>
                            <p className={cx(ArticlesCss.info, ArticlesCss.jora__info)}>چون جورا مارکت یک بازارچه مستقیم اینترنتی است که شما می  تونید راحت خرید کنید. انواع سوغاتی های شهر های مختلف ایران از جمله سوغاتی ساخت دست مردم هر شهر و از جمله کیفیت و قیمت پایین تر بدون دخالت واسطه عرضه می کندکه شما می توانید آن ها را خرید کنید و استفاده کنید البته با خرید از جورا از تولید کننده </p>
                            <div className={ArticlesCss.div__more} >
                                <button onClick={handleClick} className={ArticlesCss.more}> بیشتر</button>
                            </div>
                        </>
                    }
                    {show2 === 1 &&
                        <div  className={ArticlesCss.Art_desc}>
                            چون جورا مارکت یک بازارچه مستقیم اینترنتی است که شما می  تونید راحت خرید کنید. انواع سوغاتی های شهر های مختلف ایران از جمله سوغاتی ساخت دست مردم هر شهر و از جمله کیفیت و قیمت پایین تر بدون دخالت واسطه عرضه می کندکه شما می توانید آن ها را خرید کنید و استفاده کنید البته با خرید از جورا از تولید کننده حمایت زیادی خواهید کرد و افراد زیادی را نیز تشویق می کند که در حوزه ی تولید صنایع دستی فعالیت نموده و تلاش خودرا در این زمینه به ثمر برسانند.
                        </div>
                    }
                </div>

            </div>
        </Fragment>
    );
}

export default FirstArt;
