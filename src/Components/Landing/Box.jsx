import React, { Fragment } from 'react';
import BoxCss from "./Box.module.css";
import B1 from '../../Assets/img/Landing/Box/Shield Done.svg';
import B2 from '../../Assets/img/Landing/Box/Send.svg';
import B3 from '../../Assets/img/Landing/Box/Discount.svg';


const Box = () => {
    return (
        <Fragment>
            <div className="container">

                <div className={`col-12 ${BoxCss.boxes}`}>
                    <div className={`${BoxCss.box1}`}>
                        <div className={BoxCss.boxpic} >
                            <figure>
                                <img src={B1} alt="img" />
                            </figure>
                        </div>
                        <div className={BoxCss.boxtext}> 
                        <p className={BoxCss.title}>امنیت</p>
                        <p className={BoxCss.disc}>اطمینان در پرداخت آنلاین</p>
                        </div>
                    </div>


                    <div className={`${BoxCss.box1}`}>
                        <div className={BoxCss.boxpic} >
                            <figure>
                                <img src={B2} alt="img" />
                            </figure>
                        </div>
                        <div className={BoxCss.boxtext}>
                        <p className={BoxCss.title}>سریع</p>
                        <p className={BoxCss.disc}>ارسال در سریعترین زمان ممکن</p>
                        </div>
                    </div>


                    <div className={`${BoxCss.box1}`}>
                        <div className={BoxCss.boxpic} >
                            <figure>
                                <img src={B3} alt="img" />
                            </figure>
                        </div>
                        <div className={BoxCss.boxtext}> 
                        <p className={BoxCss.title}>تخفیف</p>
                        <p className={BoxCss.disc}>تخفیف های ویژه برای خرید</p>
                        </div>
                    </div>

                </div>

            </div>

        </Fragment>
    );
}

export default Box;
