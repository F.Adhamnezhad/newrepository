import React, { Fragment } from 'react';
import ProductHeader from '../Products/ProductHeader';
import CommentCss from './Comments.module.css';
import CommentsSlider from './Slider/CommentsSlider';
const Comments = (props) => {
    return (
        <Fragment>
             <section className={`container-fluid ${CommentCss.main__div}`}>
                <ProductHeader title="نظرات" subTitle="نظرات مشتریان جورا مارکت"/>
                <CommentsSlider url={props.url}/>
            </section>
        </Fragment>
    );
}

export default Comments;
