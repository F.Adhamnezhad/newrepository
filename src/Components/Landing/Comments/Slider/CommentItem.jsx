import React, { Fragment } from 'react';
import CommentItemCss from './CommentItem.module.css';
import UserPic from '../../../../Assets/img/Landing/Comments/Ellipse10.svg';
import Star from '../../../../Assets/img/Landing/Comments/Group 7.svg';
const CommentItem = (props) => {
    return (
        <Fragment>
            <div className={CommentItemCss.card}>
                <div className={CommentItemCss.card__items}>
                    <div className={CommentItemCss.user__item}>
                    <div className={CommentItemCss.user__img}>
                        <figure>
                            <img src={UserPic} alt="..." />
                        </figure>
                    </div>
                    <span className={CommentItemCss.user__name}> {props.name} </span>
                    </div>

                    <span className={CommentItemCss.stars}>
                    <figure>
                            <img src={Star} alt="..." />
                     </figure>
                    </span>
                </div>
                <div className={CommentItemCss.card__comment}>
                   <span > {props.text} </span>
                </div>

            </div>
        </Fragment>
    );
}

export default CommentItem;
