import React, { Fragment, useState, useEffect } from 'react';
import CommentSliderCss from './CommentsSlider.module.css'
import cx from 'classnames';
import Slider from 'react-slick';
import CommentItem from '../../Comments/Slider/CommentItem';
import Axios from 'axios';

function SampleNextArrow(props) {
    const { onClick } = props;
    return (
        <div onClick={onClick} className={CommentSliderCss.arrows}></div>
    );
}

function SamplePrevArrow(props) {
    const { onClick } = props;
    return (
        <p onClick={onClick} className={cx(CommentSliderCss.arrows, CommentSliderCss.leftArrow)}></p>
    );
}

const CommentsSlider = (props) => {

    const [data, setData] = useState([]);
    const URL = props.url;
    const GetComments = async () => {
        let res = await Axios.get(`${URL}/comment/list`);
        setData(res.data);
        console.log("DataComment", res.data);
    }
    useEffect(() => {
        GetComments();
    }, []);
    var settings = {
        rtl: true,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slideToScroll: 1,
        arrows: true,
        autoplay:true,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,

                }
            },

            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]
    }
    return (
        <Fragment>
            <div className={`container ${CommentSliderCss.slider}`}>

                <Slider className={`col-12 ${CommentSliderCss.sliderMain}`} {...settings}>
                    {data.map(i => {
                        return <CommentItem key={i.id} text={i.text} name={i.name} />
                    })}
                </Slider>

            </div>
        </Fragment>
    );
}

export default CommentsSlider;
