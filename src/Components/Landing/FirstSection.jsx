import React, { Fragment } from 'react';
import FirstCss from "./FirstSection.module.css";

const FirstSection = () => {
    return (
        <Fragment>
            <div className={`container ${FirstCss.main}`}>
                <div className={`col-12 ${FirstCss.div}`}>

                </div>
            </div>
            <div className={`container-fluid ${FirstCss.mobile}`}>
                <div className={`col-12 ${FirstCss.div}`}>

                </div>
            </div>

        </Fragment>
    );
}

export default FirstSection;
