import React, { Fragment, useContext, useState, useEffect } from 'react';
import ProductCss from './Product.module.css';
import ProductHeader from './ProductHeader';
import ProductMenu from './ProductMenu';
import ProductSlider from './Slider/ProductSlider';
import Axios from 'axios';
import { useParams } from "react-router-dom";

const Product = (props) => {
    const URL = props.url;
    const [status, setStatus] = useState(0);
    const [products, setProducts] = useState([]);
    const [Id, setId] = useState("");
    const [style, setStyle] = useState("#fff");
    const handleClick = (c)=>{
        setId(c.id);
        setStatus(1);
        setStyle("ffefdf");
    }
    const [cat, setCat] = useState([]);
    const GetCategories = async () => {
        let res = await Axios.get(`${URL}/category/list`);
        setCat(res.data);
        console.log("Datacategory", res.data);
    }
    useEffect(() => {
        GetCategories();
    }, []);

    const GetProducts = async () => {
        let res = await Axios.get(`${URL}/product/list`);
        setProducts(res.data);
    }
    useEffect(() => {
        GetProducts();
    }, []);

    const catData = products.filter((pr, index) => {
        return pr.category.map(c => c.id) == Id ; 
    })
console.log("styleee" , style);

    return (
        <Fragment>
            <section className={`container-fluid ${ProductCss.main_div}`}>
                <ProductHeader title="محصولات" subTitle="بخشی از محصولات جورا مارکت" />
                <ProductMenu back={style} cat={cat} url={URL} handleClick={handleClick}/>
                {status === 0 &&  <ProductSlider url={URL} Data={products} />}
                {status === 1 && <ProductSlider url={URL} Data={catData} />}   
            </section>
        </Fragment>
    );
}

export default Product;
