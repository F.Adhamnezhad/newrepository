import React, { Fragment , useState} from 'react';
import ProductCss from './Product.module.css';

const ProductHeader = (props) => {
    const [title, setTitle] = useState("");
    const [subTitle, setSubTitle] = useState("");
    return (
        <Fragment>
            
                <div className={`col-12 ${ProductCss.header}`}>
                    <header className={ProductCss.title}>
                        {props.title}
                    </header>
                    <header className={ProductCss.subtitle}>
                    {props.subTitle}
                    </header>
                </div>
           
        </Fragment>
    );
}

export default ProductHeader;
