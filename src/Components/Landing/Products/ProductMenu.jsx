import React, { Fragment, useEffect, useState } from 'react';
import ProductMenuCss from './ProductMenu.module.css';
import Slider from 'react-slick';
import { Link } from 'react-router-dom';

const ProductMenu = (props) => {
    const cat = props.cat;
    var settings = {
        rtl: true,
        infinite: false,
        speed: 300,
        slidesToShow: 8,
        autoplay: true,
        slideToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                }
            },

            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            }
        ]
    }

    return (
        <Fragment>
            <nav className="d-flex justify-content-center">
                <div className={`container ${ProductMenuCss.slider}`}>

                    <Slider className={`col-12 col-md-9 ${ProductMenuCss.sliderMain}`} {...settings}>
                        {cat.map(c =>
                            <button key={c.id} onClick={() =>props.handleClick(c)} className={ProductMenuCss.item}>{c.name}</button>
                        )}
          
                    </Slider>

                </div>
            </nav>
        </Fragment>
    );
}

export default ProductMenu;
