import { Fragment, React, useState, useEffect, useContext } from 'react';
import ProductItemCss from './ProductItem.module.css';
import { Link } from 'react-router-dom';

const ProductItem = ({ images, prices, id, name, add , url }) => {
    const val = images.filter(i => i.default === true);
    const img = val[0] ? val[0].image: images[0].image ;
    {console.log("imagessss" ,img )}
    const val2 = prices.filter(i => i.default === true);
    const pr = val2[0] ? val2[0].price: prices[0].price ;

    return (
        <Fragment>
            <div className={ProductItemCss.card}>
                <div className={ProductItemCss.card__image}>
                    <figure>
                        <Link to={`/productDetail/${id}`}><img src={`${url+img}`} alt="..." /></Link>
                    </figure>
                   { console.log("imggggg" , img)}
                </div>
                <div className={ProductItemCss.card__title}><Link className={ProductItemCss.Link} to={`/productDetail/${id}`}>{name}</Link></div>
                <div className={ProductItemCss.card__items}>
                    <button onClick={add} className={ProductItemCss.btn__add}>+</button>
                    <span className={ProductItemCss.price}> {pr} ریال</span>
                    {console.log("price", prices.filter(i => i.default === true))}
                </div>
            </div>
        </Fragment>
    );
}

export default ProductItem;
