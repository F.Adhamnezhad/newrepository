import { Fragment, React, useState, useEffect, useContext } from 'react';
import ProductSliderCss from "./ProductSlider.module.css";
import cx from 'classnames'
import ProductItem from './ProductItem';
import Slider from 'react-slick';
import { CartContext } from '../../../ShoppingCart/CartContext';
import { ToastContainer, toast, Zoom } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure()
function SampleNextArrow(props) {
    const { onClick } = props;
    return (
        <Fragment>
            <div onClick={onClick} className={ProductSliderCss.arrows}></div>
        </Fragment>
    );
}

function SamplePrevArrow(props) {
    const { onClick } = props;
    return (
        <Fragment>
            <div onClick={onClick} className={cx(ProductSliderCss.arrows, ProductSliderCss.leftArrow)}></div>
        </Fragment>
    );
}


const ProductSlider = (props) => {
    const products = props.Data;
    const [cart, setCart] = useContext(CartContext);
    const addToCart = (product) => {
        let newCart = [...cart];
        let itemInCart = newCart.find(
            (item) => product.id === item.id
        );

        if (itemInCart) {
            itemInCart.qty++;
        } else {
            itemInCart = {
                id:product.id,
                priceId:product.prices.filter(i=> i.default == true).map(d=> d.price) ? product.prices.filter(i=> i.default == true).map(d=> d.id) : product.prices[0].id,
                qty: 1,
                price:product.prices.filter(i=> i.default == true).map(d=> d.price) ? product.prices.filter(i=> i.default == true).map(d=> d.price) : product.prices[0].price,
                images:product.images,
                name:product.name,
                discount:product.discount,
                prices:product.prices,
                details:product.details,
                value:product.prices.filter(i=> i.default == true).map(d=> d.price) ? product.prices.filter(i=> i.default == true).map(d=> d.name) : product.prices[0].name,
            };
            newCart.push(itemInCart);

        }
        setCart(newCart);
        console.log("new item", newCart);
        toast(<div className={ProductSliderCss.toast}> محصول به سبد خرید اضافه شد</div>, {
            position: "top-center",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            transition: Zoom
        });
    };

    var settings = {
        rtl: true,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        autoplay:true,
        slideToScroll: 2,
        arrows: true,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1.5,
                }
            },

            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }

        ]
    }

    return (
        <Fragment>
            <div className={`container ${ProductSliderCss.slider}`}>
                <Slider className={`col-12 ${ProductSliderCss.sliderMain}`} {...settings}>
                    {products.map(i => {
                        return <ProductItem url={props.url} add={() => addToCart(i)} id={i.id} key={i.id} name={i.name} images={i.images} prices={i.prices} weight={i.weight} />
                    })}

                </Slider>
            </div>
        </Fragment>
    );
}

export default ProductSlider;
