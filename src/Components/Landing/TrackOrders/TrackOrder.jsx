import React, { Fragment, useState } from 'react';
import TrackOrderCss from './TrackOrder.module.css';
import ProductHeader from '../Products/ProductHeader';
import { Link } from 'react-router-dom';
import TrackLanInput from '../../Common/TrackLanInput';
import TrackOrderValidation from '../../../Core/Validations/TrackOrderValidation';
import { Formik, Form, } from 'formik';
import Axios from 'axios';
import { useHistory } from "react-router-dom";
import { ToastContainer, toast, Zoom } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure()
const TrackOrder = (props) => {
    const [value, setValue] = useState({ id: "" });
    const [error, setError] = useState(false);
    const history = useHistory();
    const handleSubmit = async (newData) => {
        var FormData = require('form-data');
        var data1 = new FormData();
        data1.append('id', newData.id);
        var config = {
            method: 'post',
            url: 'http://api.jora.market/order/tracking',
            data: data1
        };

        Axios(config)
            .then(function (response) {
               (response.data.error) && 
                toast(<div className={TrackOrderCss.toast}> {response.data.message} </div>, {
                    position: "top-center",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    transition: Zoom
                })
                 
                console.log(JSON.stringify(response.data));
                !(response.data.error) && 
                    history.push(
                        {
                            pathname: "/trackordersLanding",
                            state: response.data
                        }

                    );
            })

            .catch(function (error) {
                console.log(error);
            });
    }
    return (
        <Fragment>
            <div className={`container ${TrackOrderCss.main}`}>
                <ProductHeader title=" پیگیری سفارشات " subTitle="پیگیری سریع سفارشات شما" />
                <div className={`col-12 ${TrackOrderCss.item}`}>
                    <div className={`col-lg-3 col-md-4 ${TrackOrderCss.contact}`}>
                        <div className={TrackOrderCss.contact__ul}>
                            <ul>
                                <li className={TrackOrderCss.first}> به کمک نیاز دارید؟</li>
                                <li className={TrackOrderCss.second}>
                                    <span>با ما تماس بگیرید</span>
                                    <div className={TrackOrderCss.div}>
                                        <span>091</span>
                                        <p>12345678</p>
                                    </div>
                                    <div className={TrackOrderCss.div}>
                                        <span>091</span>
                                        <p>12345678</p>
                                    </div>
                                </li>
                                <li className={TrackOrderCss.third}><Link className={TrackOrderCss.link} to="/contactus/faq">سوالات متداول</Link></li>
                            </ul>

                        </div>
                    </div>
                    <div className={`col-md-5 col-8 ${TrackOrderCss.order}`}>
                        <span>جهت پیگیری سفارشات شماره پیگیری که برای شما ارسال شده را وارد نمایید. درصورت نداشتن شماره پیگیری با ما تماس بگیرید.</span>
                        <p>شماره پیگیری</p>

                        <Formik
                            initialValues={value}
                            validationSchema={TrackOrderValidation}
                            onSubmit={(newData) => { handleSubmit(newData) }}
                            enableReinitialize="true"
                        >
                            {() => (
                                <Form className={TrackOrderCss.form}>
                                    <TrackLanInput
                                        name="id"
                                        type="number"
                                        placeholder="لطفا کد پیگیری سفارش را وارد نمایید"
                                    />
                                    <button type="submit"> پیگیری سفارش </button>
                                </Form>
                            )}
                        </Formik>

                    </div>
                    <div className={`col-lg-3 col-md-2 col-4 ${TrackOrderCss.picture}`}></div>
                </div>
            </div>
        </Fragment>
    );
}

export default TrackOrder;
