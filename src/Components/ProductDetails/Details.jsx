import React, { Fragment, useContext, useState, useEffect } from 'react';
import DetailsCss from './Details.module.css';
import Left_Detail from './Left_Detail/Left_Detail';
import Right_Detail from './RightDetail/Right_Detail';
import RouteHeader from '../Common/RouteHeader';
import { useParams } from "react-router-dom";
import BigImageSlider from './RightDetail/BigImageSlider';
import Modal from 'react-modal';
import Axios from 'axios';

const ProductDetails = (props) => {
    const { productId } = useParams();
    const [modalIsOpen, setIsOpen] = useState(false);
    const [products, setProducts] = useState([]);
    const URL = props.url;
    const GetProducts = async () => {
        let res = await Axios.get(`${URL}/product/list`);
        setProducts(res.data);
        console.log("dataaa Res.Data", res.data)
    }
    console.log("data Products", products)

    useEffect(() => {
        GetProducts();
    }, []);
    const details = products.filter((pr, index) => {
        return pr.id == productId;
    })
    function openModal() {
        setIsOpen(true);
    }
    function afterOpenModal() {

    }
    function closeModal() {
        setIsOpen(false);
    }
    const handleClick = () => {
        setIsOpen(true);
    }
    return (
        <Fragment>
            {details.map(p => (
                <Modal
                    key={p.id}
                    isOpen={modalIsOpen}
                    ariaHideApp={false}
                    onAfterOpen={afterOpenModal}
                    onRequestClose={closeModal}
                    contentLabel="Example Modal"
                    className={DetailsCss.modalbody}
                >

                    <div key={p.id} className={DetailsCss.modalItem}>
                        <button className={DetailsCss.btnClose} onClick={closeModal}></button>
                        <div><BigImageSlider url={URL} image={p.images} /></div>
                    </div>
                </Modal>
            ))}
            <div className={`col-12 ${DetailsCss.main__div}`} >
                <RouteHeader pageTitle="جزئیات محصول" />
            </div>
            <>
                <div className={`container ${DetailsCss.main}`}>
                    {details.map(p => (
                        <Right_Detail url={URL} key={p.id} onClick={handleClick} image={p.images} />
                    ))}
                    {console.log("imgarr", details.img)}
                    <Left_Detail Data={products} url={props.url} />
                </div>
                <div className={`container_fluid ${DetailsCss.mainMobile}`}>
                    {details.map(p => (
                        <Right_Detail url={URL} key={p.id} onClick={handleClick} image={p.images} />
                    ))}
                </div>
                <div className={`container ${DetailsCss.LeftMob}`}>
                   <Left_Detail Data={products} url={props.url} /> 
                </div>

            </>
        </Fragment>
    );
}

export default ProductDetails;
