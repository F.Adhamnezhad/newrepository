import React, { useState, Fragment } from 'react';
import Left_DetailCss from './Left_Detail.module.css';
import ProductInfo from './ProductInfo';
import ProductTab from './Tab/ProductTab';
import { useParams } from "react-router-dom";
import CommentsSlider from '../../Landing/Comments/Slider/CommentsSlider';

const Left_Detail = (props) => {
    const products = props.Data;
    const { productId } = useParams();
    const tabData = products.filter((pr, index) => {
       return  pr.id == productId;
    })
    const Data = tabData.map(i=>i.description);
    const Data2 = Data.map((i,index) => i)
    console.log("tabInfo" , tabData)
    const [tabs, settabs] = useState(
        [
            {
                id: 1,
                name: "توضیحات",
                text:tabData.map( t=> <div key={t.id}>{t.description}</div>) 
            },
            {
                id: 2,
                name: "مشخصات",
                text:tabData.map(i=> {
                        <div key={i.id}>{i.details.map(d=>{
                         return (<div key={d.id}> <span >{d.name}: </span> <span> {d.value} </span> </div>)})}</div>
                })

            },
            {
                id: 3,
                name: "نظرات",
                text: <CommentsSlider url={props.url} />
            }
        ]
    );
    return (
        <Fragment>

            <div className={` col-md-8 col-12 ${Left_DetailCss.main__left}`}>
                <ProductInfo Data={products} />
             {/*    <ProductTab tabs={tabs} /> */}
            </div>

        </Fragment>
    );
}

export default Left_Detail;
