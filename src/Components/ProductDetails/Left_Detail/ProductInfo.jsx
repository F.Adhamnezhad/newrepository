import React, { Fragment, useContext, useState } from 'react';
import ProductInfoCss from './ProductInfo.module.css';
import { useParams } from "react-router-dom";
import ProductValue from './ProductValue';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { CartContext } from '../../ShoppingCart/CartContext';
import { ToastContainer, toast, Zoom } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ProductInfo = (props) => {
    const Data = props.Data;
    const [status, setStatus] = useState(0);
    const [cart, setCart] = useContext(CartContext);
    const { productId } = useParams();
    const [Id, setId] = useState("");
    const [count, setCount] = useState(1);

    const handleClick = (v) => {
        setId(v.id);
        setStatus(1);
    }
    const addToCart = (product) => {

        let newCart = [...cart];
        let itemInCart = newCart.find(
            (item) => Id ? product.id === item.id &&  item.priceId ===  Id : product.id === item.id 
        );

        if (itemInCart) {
            itemInCart.qty++;
        } else {
            itemInCart = {
                id: product.id,
                priceId:
                    status === 0 ? product.prices.filter(i => i.default == true).map(d => d.price) ? product.prices.filter(i => i.default == true).map(d => d.id) : product.prices[0].id :
                        product.prices.filter(x => x.id == Id).map(c => c.id),
                qty: 1,
                price: status === 0 ? product.prices.filter(i => i.default == true).map(d => d.price) ? product.prices.filter(i => i.default == true).map(d => d.price) : product.prices[0].price :
                    product.prices.filter(x => x.id == Id).map(c => c.price),
                images: product.images,
                name: product.name,
                discount: product.discount,
                prices: product.prices,
                details: product.details,
                value: Id ? product.prices.filter(x => x.id == Id).map(z => z.name) : product.prices.filter(i => i.default == true).map(d => d.price) ? product.prices.filter(i => i.default == true).map(d => d.name) : product.prices[0].name,
            };
            newCart.push(itemInCart);
        }
        setCart(newCart);
        console.log("new item", newCart);
        toast(<div className={ProductInfoCss.toast}> محصول به سبد خرید اضافه شد</div>, {
            position: "top-center",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            transition: Zoom
        });
    };

    const details = Data.filter((pr, index) => {
        return pr.id == productId;
    })
    const DataPrice = details.map(i => i.prices)
    /* const val2 = DataPrice[0].filter(i => i.default === true); */
    /*  const val2 = details[0].prices.filter(i => i.default === true); */
    /* const pr = val2[0] ? val2[0].price: details.prices[0].price ;  */

    return (
        <Fragment>

            {details.map(p => (
                <div key={p.id} className={`col-12 ${ProductInfoCss.first__left}`}>
                    <p>{p.name}</p>
                    <span>(4نظر 5 از 5)</span>
                    <div className={ProductInfoCss.sec1}>
                        <div> قیمت:</div>
                    </div>
                    <div className={ProductInfoCss.sec2}>
                        {status === 0 &&
                            <p> {p.prices.filter(i => i.default == true).map(d => d.price) ? p.prices.filter(i => i.default == true).map(d => d.price) : p.prices[0].price} ریال</p>
                        }
                        {status === 1 &&
                            <p> {p.prices.filter(x => x.id == Id).map(c => c.price)}ریال</p>
                        }
                        <button onClick={() => addToCart(p)}>افزودن به سبد خرید</button>
                    </div>
                    <ProductValue handleClick={handleClick} value={p.prices} />

                    <div className={ProductInfoCss.stick}>
                        <button onClick={() => addToCart(p)}>
                            {status === 0 &&
                                <p>خرید | {p.prices.filter(i => i.default == true).map(d => d.price) ? p.prices.filter(i => i.default == true).map(d => d.price) : p.prices[0].price} ریال</p>
                            }
                            {status === 1 &&
                                <p>خرید | {p.prices.filter(x => x.id == Id).map(c => c.price)}ریال</p>
                            }
                        </button>
                    </div>
                </div>

            ))}
        </Fragment>
    );
}

export default ProductInfo;
