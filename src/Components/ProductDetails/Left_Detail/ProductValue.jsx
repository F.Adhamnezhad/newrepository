import React, { Fragment, useContext } from 'react';
import ProductValueCss from './ProductValue.module.css';

const ProductValue = ({ value , handleClick }) => {

    return (
        <Fragment>
            {value.name=="وزن"}
            <div className={`col-12 ${ProductValueCss.second__left}`}>
                <p>مقدار:</p>
                {value.map((v) => (
                    <button onClick={()=>handleClick(v)} key={v.id}> {v.name} </button>
                ))}
            </div>

        </Fragment>
    );
}

export default ProductValue;
