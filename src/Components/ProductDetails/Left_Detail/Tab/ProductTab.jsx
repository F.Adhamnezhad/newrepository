import React, { Fragment, useState ,useContext} from 'react';
import ProductTabCss from './ProductTab.module.css';
import cx from 'classnames';
import TabNav from './TabNav';
import Tab from './Tab';


const ProductTab = (props) => {

    const [activeTabId, setActiveTab] = React.useState(props.tabs[0].id);

    const activeTab = React.useMemo(() => (
        props.tabs.find((tab) => (
            tab.id === activeTabId
        ))
    ), [activeTabId, props.tabs]);


    return (
        <Fragment>
            <div className={`col-12 ${ProductTabCss.third__left}`}>
                <div className={ProductTabCss.tabs}>
                    <TabNav tabs={props.tabs} onNavClick={setActiveTab}
                        activeTabId={activeTabId} />
                    <Tab tab={activeTab} />
                </div>
            </div>
        </Fragment>
    );
}

export default ProductTab;
