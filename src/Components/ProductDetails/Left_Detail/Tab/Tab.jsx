import React, { Fragment } from 'react';
import ProductTabCss from './ProductTab.module.css';

const Tab = (props) => {
    return (
        <Fragment>
            <div className={ProductTabCss.tabs__content}>
                {props.tab.text}
            </div>
        </Fragment>
    );
}

export default Tab;
