import React, { Fragment, useState } from 'react';
import TabNavCss from './TabNav.module.css';
import cx from "classnames"

const TabNav = (props) => {


    return (
        <Fragment>
            <ul className={TabNavCss.tabs__nav}>
                {props.tabs.map((item) => (
                    <li key={item.id}>
                        {(props.activeTabId === item.id) ?
                    <button onClick={() => props.onNavClick(item.id)} className={cx(TabNavCss.tabs__button)}>{item.name}</button> 
                    :
                    <button  onClick={() => props.onNavClick(item.id)} className={TabNavCss.tabs__item}> {item.name} </button>
                    }
                    </li>
                ))}
            </ul>
        </Fragment>
    );
}

export default TabNav;
