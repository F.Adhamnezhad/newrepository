import React from 'react';
import BigImageSliderCss from './BigImageSlider.module.css';
import Slider from "react-slick";
import cx from 'classnames';

function SampleNextArrow(props) {
    const { onClick } = props;
    return (
        <div onClick={onClick} className={BigImageSliderCss.arrows}></div>
    );
}

function SamplePrevArrow(props) {
    const { onClick } = props;
    return (
        <div onClick={onClick} className={cx(BigImageSliderCss.arrows, BigImageSliderCss.leftArrow)}></div>
    );
}

const BigImageSlider = ({ url , image }) => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />
    };
    return (
        <div className={BigImageSliderCss.div_asli}>
            <Slider {...settings}>
                {image.map(i => (
                    <div key={i.id} className={BigImageSliderCss.imagediv} > <img className={BigImageSliderCss.image} src={url+i.image} /> </div>
                ))}
            </Slider>
        </div>
    );
}

export default BigImageSlider;
