import React, { Fragment } from 'react';
import Slider from "react-slick";
import SliderCss from './ImageSlider.module.css';

const imageSlider = ({ url,image , onClick }) => {
    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 920,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,

                }
            }

        ]
    };
    return (

        <Fragment>
            <div  className={SliderCss.maindiv}>
                <Slider {...settings}>
                    {image.map(i  => (
                        <div key={i.id} onClick={onClick} className={SliderCss.imagediv} > <img src={url+i.image} /> </div>
                    ))}
                </Slider>
            </div>
        </Fragment>
    );
}

export default imageSlider;
