import React, { Fragment, useState } from 'react';
import MobImgSliderCss from './MobImgSlider.module.css'
import Slider from "react-slick";
import Pic from '../../../Assets/img/Landing/Products/slider/zanbil-hasiri-gilanbazar 1.svg'
import {
    MDBCarousel,
    MDBCarouselInner,
    MDBCarouselItem,
    MDBCarouselElement,
    MDBCarouselCaption,
} from 'mdb-react-ui-kit';

const Images = [
    { id: 0, src: Pic },
    { id: 1, src: Pic },
    { id: 2, src: Pic },
    { id: 3, src: Pic },
    { id: 4, src: Pic }
];

const MobImgSlider = ({ image, url }) => {

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll:1
    };

    return (
        <Fragment>
            <div className={MobImgSliderCss.div_asli}>
                <Slider {...settings}>
                    {image.map(i => (
                        <div key={i.id} className={MobImgSliderCss.imagediv} > <img className={MobImgSliderCss.image} src={url + i.image} /> </div>
                    ))}
                </Slider>
            </div>

        </Fragment>
    );
}

export default MobImgSlider;
