import React, { Fragment, useContext, useState } from 'react';
import Right_DetailCss from './Right_Detail.module.css';
import cx from 'classnames';
import RoutHeader from '../../Common/RouteHeader';
import MobImgSlider from './MobImgSlider';
import ImageSlider from './ImageSlider';

const Right_Detail = ({ url, image, onClick }) => {
    const val = image.filter(i => i.default === true);
    const img = val[0] ? val[0].image: image[0].image ;
    return (
        <>
            <Fragment>
                <div className={`col-md-4  ${Right_DetailCss.main__right}`}>
                    <div onClick={onClick} className={`col-12 ${Right_DetailCss.big_img}`}>
                        <figure>
                            <img src={url+img} alt="..." />
                        </figure>
                    </div>
                    <ImageSlider url={url} onClick={onClick} image={image} />
                </div>
                {/* .................. Mobile Mode Slider.............................. */}

                <div className={Right_DetailCss.mobile__h}>
                    <RoutHeader pageTitle="جزئیات محصول" />
                </div>
                <div className={Right_DetailCss.mobile__slider}>
                    <span className={Right_DetailCss.back}> بازگشت</span>
                    <MobImgSlider url={url} image={image} />
                </div>
            </Fragment>

        </>

    );

}

export default Right_Detail;
