import React, { Fragment, useState, useEffect } from 'react';
import AccordionCss from './Accordion.module.css';
import PriceRange from './PriceRange';
import cx from 'classnames';
import Axios from 'axios'
const menu = [
    { id: 0, value: 'دسته بندی ها' },
    { id: 1, value: 'امتیاز محصول' }
]
const item = [
    { id: 0, title: "محصولات" },
    { id: 1, title: "صنایع دستی" },
    { id: 2, title: "محصولات" }
]
const Accordion = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    const [isOpen2, setIsOpen2] = useState(false);
    const [isOpen3, setIsOpen3] = useState(false);
    const URL = props.url;
    const cat = props.cat;
    return (
        <Fragment>

            <div className={AccordionCss.accordion}>

                {!isOpen && (<div className={AccordionCss.accordion_header} onClick={() => !isOpen ? setIsOpen(true) : setIsOpen(false)}>
                    دسته بندی محصول
                </div>)}
                {isOpen && (<>
                    <div onClick={() => !isOpen ? setIsOpen(true) : setIsOpen(false)} className={AccordionCss.accordion_header__span} > دسته بندی محصول </div>

                    <div className={AccordionCss.accordion_body}>
                        <ul>
                            {cat.map(c =>
                                <li key={c.id}>
                                    <button onClick={()=>props.handleClick(c)}>{c.name} </button>
                                </li>
                            )}
                        </ul>
                    </div>
                </>
                )}

            </div>

            <div className={cx(AccordionCss.accordion, AccordionCss.accordion_t)}>
                {!isOpen2 && (<div className={AccordionCss.accordion_header} onClick={() => !isOpen2 ? setIsOpen2(true) : setIsOpen2(false)}>
                    امتیاز محصول
                </div>)}
                {isOpen2 && (<>
                    <div onClick={() => !isOpen2 ? setIsOpen2(true) : setIsOpen2(false)} className={AccordionCss.accordion_header__span} > امتیاز محصول </div>

                    <div className={AccordionCss.accordion_body}>
                        <ul>
                            <li onClick={{}}>
                                <a href="#">محصولات</a>
                            </li>
                            <li onClick={{}}>
                                <a href="#">صنایع دستی</a>
                            </li>
                            <li onClick={{}}>
                                <a href="#">محصولات هنری</a>
                            </li>
                        </ul>
                    </div>
                </>
                )}

            </div>


            <div className={cx(AccordionCss.accordion, AccordionCss.accordion_t)}>
                {!isOpen3 && (<div className={AccordionCss.accordion_header} onClick={() => !isOpen3 ? setIsOpen3(true) : setIsOpen3(false)}>
                    محدوده قیمت
                </div>)}
                {isOpen3 && (<>
                    <div onClick={() => !isOpen3 ? setIsOpen3(true) : setIsOpen3(false)} className={AccordionCss.accordion_header__span} > محدوده قیمت </div>

                    <div className={AccordionCss.accordion_body}>
                        <PriceRange min={50000} max={400000} />
                    </div>
                </>
                )}

            </div>

        </Fragment>
    );
}

export default Accordion;




