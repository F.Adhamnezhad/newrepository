import React, { Fragment, useEffect, useState, useContext } from 'react';
import ProductItem from '../Landing/Products/Slider/ProductItem';
import PaginationCss from './PaginationList.module.css';
import { CartContext } from '../ShoppingCart/CartContext';
import { ToastContainer, toast, Zoom } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const PaginationList = (props) => {
    const [showBtn, setShowBtn] = useState(true);
    const products = props.Data;
    const [cart, setCart] = useContext(CartContext);
    const [visibility, setVisibility] = useState(12);

    const ShowMoreItems = () => {
        setVisibility((prev) => prev + 12)
    }

    const addToCart = (product) => {
        console.log("Iddd", product.id);
        let newCart = [...cart];
        let itemInCart = newCart.find(
            (item) => product.id === item.id 
        );
        if (itemInCart) {
            itemInCart.qty++;
        } else {
            itemInCart = {
                id: product.id,
                priceId: product.prices.filter(i => i.default == true).map(d => d.price) ? product.prices.filter(i => i.default == true).map(d => d.id) : product.prices[0].id,
                qty: 1,
                price: product.prices.filter(i => i.default == true).map(d => d.price) ? product.prices.filter(i => i.default == true).map(d => d.price) : product.prices[0].price,
                images: product.images,
                name: product.name,
                discount: product.discount,
                prices: product.prices,
                details: product.details,
                value:product.prices.filter(i=> i.default == true).map(d=> d.price) ? product.prices.filter(i=> i.default == true).map(d=> d.name) : product.prices[0].name
            };
            newCart.push(itemInCart);
        }
        setCart(newCart);
        console.log("new item", newCart);
        toast(<div className={PaginationCss.toast}> محصول به سبد خرید اضافه شد</div>, {
            position: "top-center",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            transition: Zoom
        });
    };

    return (
        <Fragment>
            <div className={`container ${PaginationCss.main}`}>
                { console.log("paginationdataaaaaaaaaaaaaaaaa", products)}
                {products.map(i => {
                        return <ProductItem url={props.url} add={() => addToCart(i)} id={i.id} key={i.id} name={i.name} images={i.images} prices={i.prices} weight={i.weight} />
                    })}

                {/* https://www.youtube.com/watch?v=Ka3OQpwqxXA */}

                {products.length > visibility &&
                    <button onClick={ShowMoreItems} className={PaginationCss.button} >نمایش بیشتر</button>
                }
            </div>

        </Fragment>
    );
}

export default PaginationList;
