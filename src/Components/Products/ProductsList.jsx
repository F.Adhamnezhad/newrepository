import React, { Fragment, useState, useContext, useEffect } from 'react';
import RoutHeader from '../Common/RouteHeader';
import Accordion from './AccordionMenu/Accordion';
import PaginationList from './PaginationList';
import ProductListCss from './ProductsList.module.css';
import SearchBox from './SearchBox';
import Sorting from './Sorting';
import { useParams } from "react-router-dom";
import Axios from 'axios';

const ProductsList = (props) => {
    const [isOpen, setIsOpen] = useState("");
    const [status, setStatus] = useState(0);
    const [Id, setId] = useState("");
    const [products, setProducts] = useState([]);
    const URL = props.url;
    const [cat, setCat] = useState([]);
    const GetCategories = async () => {
        let res = await Axios.get(`${URL}/category/list`);
        setCat(res.data);
        console.log("Datacategory", res.data);
    }
    useEffect(() => {
        GetCategories();
    }, []);
    console.log("storpage", cat)

    const handleClick = (i) => {
        setId(i.id);
        setStatus(1);
        setIsOpen(false);
    }
    const GetProducts = async () => {
        let res = await Axios.get(`${URL}/product/list`);
        setProducts(res.data);
        console.log("DataProvider", res.data);
    }
    useEffect(() => {
        GetProducts();
    }, []);

    const count = products.length;
    const [sort, setSort] = useState("");

    const catData = products.filter((pr, index) => {
        return pr.category.map(c => c.id) == Id;
    })
    const handleSort = () => {
        alert("hi");
        /*  https://www.youtube.com/watch?v=e4ajd_fINX8 */
    }
    return (
        <Fragment>
            <div className={`container-fluid ${ProductListCss.main__container}`}>
                <div className={`col-12 ${ProductListCss.main__div}`} >
                    <RoutHeader link="/products" pageTitle="محصولات" />
                </div>
                <div className={`container ${ProductListCss.content}`}>
                    <div className={` col-lg-3 col-md-4 col-12 ${ProductListCss.nav__right}`}>
                        <SearchBox setIsOpen={setIsOpen} handleClick={handleClick} cat={cat} />
                    </div>
                    <div className={`col-lg-9 col-md-8 col-11 ${ProductListCss.nav__left}`}>
                        <div className={` col-lg-5 col-md-6 col-6 ${ProductListCss.nav__left__Right}`}>
                            <p> لیست محصولات</p>
                            {status === 1 &&
                                catData.length > 20 &&
                                <div>
                                    20+
                                </div>
                            }
                            {status === 0 &&
                                products.length > 20 &&
                                <div>
                                    20+
                                </div>
                            }
                        </div>
                        <div className={`col-lg-4 col-md-4 col-6 ${ProductListCss.nav__left__Left}`}>
                            <Sorting />
                        </div>
                    </div>
                </div>
                <div className={`container ${ProductListCss.content}`}>
                    <div className={`col-lg-3 col-md-4 col-12 ${ProductListCss.content__right}`}>
                        <Accordion handleClick={handleClick} cat={cat} url={props.url} />
                    </div>
                    <div className={`col-lg-9 col-md-8 col-11 ${ProductListCss.content__left}`}>
                        {console.log("دیتا", catData)}
                        {status == 0 &&
                            <PaginationList url={URL} Data={products} />
                        }
                        {status == 1 &&
                            <PaginationList Data={catData} />
                        }

                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default ProductsList;
