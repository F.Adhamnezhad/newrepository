import React, { Fragment , useState } from 'react';
import SearchCss from './SearchBox.module.css';

const SearchBox = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    const [close, setClose] = useState(false);
    const Data = props.cat;
    return (
        <Fragment>
            <div className={SearchCss.searchBox}>
                <input
                    className={SearchCss.search}
                    type="search"
                    placeholder="جستجو کن ..."
                />
                <span className={SearchCss.searchIcon}></span>
                <span onClick={()=>setIsOpen(!isOpen)} className={SearchCss.icon}>
                    {isOpen === true &&
                    <ul>
                        {Data.map(c =>
                            <li key={c.id}>
                                <button onClick={() => props.handleClick(c)}>{c.name} </button>
                            </li>
                        )}
                    </ul>
                    }
                </span>
            </div>
        </Fragment>
    );
}

export default SearchBox;
