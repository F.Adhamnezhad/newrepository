import React, { Fragment, useState } from "react";
import SortingCss from './Sorting.module.css'


const optionsItem = ["جدیدترین ها", "پرفروش ترین ها", "جدید ترین ها"];

export default function App() {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);
  const [options, setoptions] = useState(optionsItem);
  const toggling = () => setIsOpen(!isOpen);

  const onOptionClicked = value => () => {
    setSelectedOption(value);
    setIsOpen(false);
    console.log(selectedOption);
  };

  return (
    <Fragment>
      <div className={SortingCss.main} >
        <div className={SortingCss.container}>
          <div className={SortingCss.dropdown_header} onClick={toggling}>
            {selectedOption || "مرتب سازی"}
          </div>
          {isOpen && (
            <div>
              <ul className={SortingCss.dropdown__ul}>
                {options.map(option => (
                  <li className={SortingCss.dropdown__li} onClick={onOptionClicked(option)} key={Math.random()}>
                    {option}
                  </li>
                ))}
              </ul>
            </div>
          )}
        </div>
      </div >
    </Fragment>
  );
}