import React, { Fragment } from 'react';
import RoutHeader from '../Common/RouteHeader';
import FinalCss from './Final.module.css';
import ShoppingCartPage from './Shop_body/Steps/Step1/ShoppingCartPage';


const Final = (props) => {
    return (
        <Fragment>
            <div className="container">
                <div className={`col-12 ${FinalCss.head}`}>
                    <RoutHeader pageTitle="سبد خرید" />
                </div>
                <ShoppingCartPage url={props.url} />
            </div>
        </Fragment>
    );
}

export default Final;
