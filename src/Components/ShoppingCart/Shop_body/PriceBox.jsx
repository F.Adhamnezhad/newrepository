import { Button } from 'bootstrap';
import React, { Fragment, useState ,useContext } from 'react';
import PriceBoxCss from './PriceBox.module.css';
import cx from 'classnames';
import Shopping_btn from '../../Common/Shopping_btn';
import { Link } from 'react-router-dom';
import { CartContext } from '../CartContext';


const PriceBox = (props) => {
    const [cart, setCart] = useContext(CartContext);
    const TotalPrice = cart.reduce((acc, curr) => acc + curr.qty * curr.price, 0);
    const discountPrice = cart.reduce((acc, curr) => acc + curr.qty * curr.discount, 0) ;
    const paymentPrice= TotalPrice - discountPrice;
    const btnTitle = props.btnTitle ;
    const btnPrev = props.btnPrev;
    const btnType = props.btnType;
    const color = props.color;
    const prevColor=props.prevColor;

    return (
        <Fragment>
            <div className={PriceBoxCss.div}>
                <div className={PriceBoxCss.sec}>
                    <span className={PriceBoxCss.title}>هزینه کل:</span>
                    <span >{TotalPrice} تومان</span>
                </div>
                <div className={cx(PriceBoxCss.sec , PriceBoxCss.sec2)}>
                    <span className={PriceBoxCss.title}>مقدار تخفیف:</span>
                    <span >{discountPrice} تومان</span>
                </div>
                <div className={PriceBoxCss.dash}></div>
                <div className={PriceBoxCss.sec}>
                    <span className={PriceBoxCss.title}> قابل پرداخت:</span>
                    <span className={PriceBoxCss.price}>{paymentPrice} تومان</span>
                </div>
                {(btnTitle === "پیگیری سفارش") ? <Link to="/trackorders"><Shopping_btn color={color} handleState={props.handleStateFunc} btntype={btnType} btn_ti={btnTitle}/></Link>:<Shopping_btn color={color} handleState={props.handleStateFunc} btntype={btnType} btn_ti={btnTitle}/>}
                {(btnPrev === "بازگشت") ? <Shopping_btn color={prevColor}  handleState={props.handleBack} btntype={btnType} btn_ti={btnPrev}/> : "" }
            </div>
        </Fragment>
    );
}

export default PriceBox;
