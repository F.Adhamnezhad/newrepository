import React, { Fragment, useState } from 'react';
import NavCss from './Nav.module.css';
import cx from 'classnames';


const Nav = (props) => {

  return (
    <Fragment>
      <div className="col-12">
      <ul className={NavCss.ul}>
        <li className={NavCss.li_step} >
          <span className={props.status === 0 ? cx(NavCss.li_num, NavCss.li_select) : cx(NavCss.li_num, NavCss.li_select, NavCss.li_checked)} >1
          <div></div>
          </span>

          <span className={NavCss.text}>سبد خرید</span>
        </li>
        <li className={NavCss.li_step}>
          <span className={props.status === 0 && NavCss.li_num ||
            props.status === 1 && cx(NavCss.li_num, NavCss.liBorder)
            || props.status >= 2 && cx(NavCss.li_num, NavCss.li_select, NavCss.li_checked)}>2
          <div></div></span>
          <span className={NavCss.text}>تکمیل اطلاعات</span>
        </li>
        <li className={NavCss.li_step}>
          <span className={props.status <=1 && NavCss.li_num ||
            props.status ==2  && cx(NavCss.li_num, NavCss.liBorder) || 
            props.status ==3 && cx(NavCss.li_num, NavCss.li_select, NavCss.li_checked)}> 3 </span>
          <span className={NavCss.text}> پرداخت </span>
        </li>
      </ul>
      </div>
    </Fragment>
  );
}

export default Nav;




