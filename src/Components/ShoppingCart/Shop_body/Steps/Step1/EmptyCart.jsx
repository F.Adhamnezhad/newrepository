import React from 'react';
import { Fragment } from 'react';
import EmptyCartCss from './EmptyCart.module.css';
import { ToastContainer, toast, Zoom } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Link} from 'react-router-dom';

toast.configure()
const EmptyCart = () => {

   return (
        <Fragment>
            <div className={EmptyCartCss.maindiv}>
                <span>سبد خریدشما خالی است!</span>
                <button><Link className={EmptyCartCss.Link} to="/"> بازگشت به خانه </Link></button>
            </div>
        </Fragment>
    );
}

export default EmptyCart;
