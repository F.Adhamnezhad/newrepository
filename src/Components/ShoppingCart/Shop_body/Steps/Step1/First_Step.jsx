import { Fragment, React, useState, useEffect, useContext } from 'react';
import FirstCss from './First_Step.module.css';
import { CartContext } from '../../../CartContext';
import EmptyCart from './EmptyCart'
const First_Step = (props) => {

    const [cart, setCart] = useContext(CartContext);
    const removeFromCart = (productToRemove) => {
        setCart(
            cart.filter((product) => product !== productToRemove)
        );
    };

    const handleIncrese = (product) => {
        const exist = cart.find((x) => x.id === product.id );
            setCart(
                cart.map((x) =>
                     x.id === product.id && x.price === product.price  ? { ...exist, qty: exist.qty + 1 } : x
                )
            ); 
        }
    

    const handleDecrese = (product) => {
        const exist = cart.find((x) => x.id === product.id && x.price === product.price);
        console.log("exist" , exist)
        if (exist.qty === 1) {
            setCart(cart.filter((x) => x.id !== product.id && x.price === product.price));
        } else {
            setCart(
                cart.map((x) =>
                x.id === product.id && x.price === product.price ? { ...exist, qty: exist.qty - 1 } : x
                )
            );
        }
    }

    return (
        <Fragment> 
            {cart.map((item) => (
                <div key={item.id} className={FirstCss.main_div}>
                    <div className={FirstCss.section_1} >
                        <div className={FirstCss.pic}>
                            <figure>
                                <img src={item.images.filter(i=> i.default == true).map(c=> c.image) ? item.images.filter(i=> i.default == true).map(c=> c.image) : item.images[0].image}  alt="..." />
                            </figure>
                        </div>
                        <div className={FirstCss.info}>
                            <span className={FirstCss.title}>{item.name}</span>
                            <div className={FirstCss.part}>
                                 {(item.value && <span className={FirstCss.weight}>  {item.value} </span>)} 
                               <span className={FirstCss.price}>  {item.price}   ریال</span> 
                            </div> 
                            {(item.discount > 0) &&
                            <div className={FirstCss.part}>
                            <span className={FirstCss.discountTitle}>تخفیف:</span>
                            <span className={FirstCss.priceDiscount}> {item.discount} ریال</span>
                            </div>}
                            {console.log('discount', item.discount)}
                            <button onClick={() => removeFromCart(item)} className={FirstCss.delete}>
                                حذف
                            </button>
                        </div>
                    </div>
                    <div className={FirstCss.count}>
                        <button onClick={() => handleIncrese(item)} >+</button>
                        {item.qty}
                        <button onClick={() => handleDecrese(item)}>-</button>
                    </div>
                </div>
            ))}
        </Fragment>
    );

}
export default First_Step;
