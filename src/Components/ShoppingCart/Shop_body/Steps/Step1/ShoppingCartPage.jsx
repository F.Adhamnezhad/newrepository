import React, { Fragment, useState, useEffect, useContext } from 'react';
import PriceBox from '../../PriceBox';
import First_Step from './First_Step';
import ShopCss from './ShoppingCartPage.module.css';
import Nav from '../Nav'
import CompleteInfo from '../Step2/CompleteInfo';
import Payment from '../Step3/Payment';
import FinalStep from '../Step4/FinalStep';
import { CartContext } from '../../../CartContext';
import 'react-toastify/dist/ReactToastify.css';
import EmptyCart from './EmptyCart';

const ShoppingCartPage = (props) => {
    const [cart, setCart] = useContext(CartContext);
    const [currentStep, setcurrentStep] = useState(0);
      const [Data, setData] = useState(
        {
            fullname: '',
            address: '',
            code: '',
            phonenumber: '',
            send: '1',
            payment: '1',
            cartNumber: "",
            paymentDate: ""
        });

    const handleNextStep = (newData) => {
        setData({ ...Data, ...newData });
        setcurrentStep(prev => prev + 1);
    }

    const handleprevStep = (newData) => {
        setData(prev => ({ ...prev, ...newData }));
        setcurrentStep(prev => prev - 1);
    }

    const steps = [

        <div className={`col-12 ${ShopCss.main}`}>
            <div className={`col-md-8 col-12 ${ShopCss.right}`}>
                <Nav status={0} />
                <First_Step cart={props.cart} addToCart={props.addToCart} onRemove={props.onRemove} />
            </div>
            <div className={`col-md-4 col-12 ${ShopCss.left}`}>
                <PriceBox color="#f39237" handleStateFunc={handleNextStep} btnTitle="تایید و ادامه" />
            </div>
            <div className={ShopCss.stick}>
                <span>
                    قیمت کل : {cart.reduce((acc, curr) => acc + curr.qty * curr.price, 0) - cart.reduce((acc, curr) => acc + curr.qty * curr.discount, 0)}
                </span>
                <button onClick={handleNextStep}>
                    تایید و ادامه
                </button>
            </div>
        </div>,

        <CompleteInfo cart={cart} Data={Data} handleBack={handleprevStep} next={(newData) => { handleNextStep(newData) }} status={1} />,
        <Payment  url={props.url} Data={Data} status={2} handleBack={handleprevStep} next={(newData) => { handleNextStep(newData) }} />,
        <FinalStep  status={3} handleStateFunc={() => { }} />

    ];

    return (
        <Fragment>
            {console.log("CartLenght", cart[0])}

            {
                cart.length === 0 ? (<EmptyCart />) :
                    steps[currentStep]
            }
        </Fragment >
    );

}

export default ShoppingCartPage;

