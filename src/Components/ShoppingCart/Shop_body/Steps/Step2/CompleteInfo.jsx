import React, { Fragment, useState } from 'react';
import PriceBox from '../../PriceBox';
import Nav from '../Nav';
import Second_Step from './Second_Step';
import SecondCss from './Second_Step.module.css';
import { Formik, Form, } from 'formik';
import Input from '../../../../Common/CustomInput';
import cx from 'classnames'
import ValidateForm from '../../../../../Core/Validations/ShoppingForm'
const CompleteInfo = (props) => {
    const AllData = props.Data;
    const cart = props.cart;
    const doSubmit = (values) => {
        console.log("step1 data", values);
        props.next(values);
    }
    return (
        <Fragment>
            <Formik
                initialValues={AllData}
                validationSchema={ValidateForm}
                onSubmit={doSubmit}
                enableReinitialize="true"
            >
                {() => (
                    <Form>
                        <div className={`col-12 ${SecondCss.main}`}>
                            <div className={`col-md-8 col-12 ${SecondCss.Right}`}>
                                <Nav status={props.status} />

                                {/*    ........................................................... */}

                                <div className={SecondCss.input1}>
                                    <Input
                                        title="نام و نام خانوادگی"
                                        type="text"
                                        name="fullname"
                                        placeholder=" نام و نام خانوادگی خود را وارد نمایید"
                                    />
                                    <Input
                                        title="آدرس"
                                        type="text"
                                        name="address"
                                        placeholder="آدرس خود را وارد نمایید"
                                    />
                                </div>
                                <div className={SecondCss.input1}>
                                    <Input
                                        title="کد پستی"
                                        type="text"
                                        name="code"
                                        placeholder="کدپستی خود را وارد نمایید"
                                    />
                                    <Input
                                        title="شماره تلفن همراه"
                                        type="text"
                                        name="phonenumber"
                                        placeholder="شماره تلفن همراه خود را وارد نمایید"
                                    />
                                </div>

                                <div className={SecondCss.check_ti}>انتخاب روش ارسال:</div>
                                <div className={cx(SecondCss.input1, SecondCss.i2)}>

                                    <div className={SecondCss.group1}>
                                        <Input
                                            value="2"
                                            type="radio"
                                            name="send" />
                                        <label className={SecondCss.lbl}>تیپاکس</label>
                                    </div>

                                    <div className={SecondCss.group1}>
                                        <Input
                                            value="1"
                                            type="radio"
                                            name="send" />
                                        <label className={cx(SecondCss.lbl, SecondCss.lblpost)} htmlFor="send">پست</label>
                                    </div>

                                </div>
                                {/*................................................................... */}
                            </div>
                            <div className={`col-md-4 col-12 ${SecondCss.Left}`}>
                                <PriceBox handleStateFunc={props.handleStateFunc} handleBack={props.handleBack} prevColor="#eee" color="#f39237" btnPrev="بازگشت" btnType="submit" btnTitle="تایید و ادامه" />
                            </div>
                            <div className={SecondCss.stick}>
                                <span>
                                    قیمت کل : {cart.reduce((acc, curr) => acc + curr.qty * curr.price, 0) - cart.reduce((acc, curr) => acc + curr.qty * curr.discount, 0)}
                                </span>
                                <button onClick={props.handleStateFunc}>
                                    تایید و ادامه
                                </button>
                                <button className={SecondCss.backbtn} onClick={props.handleBack}>
                                </button>
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        </Fragment>
    );
}

export default CompleteInfo;
