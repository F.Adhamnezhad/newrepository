import React, { Fragment, useState } from 'react';
import SecondCss from './Second_Step.module.css';
import cx from 'classnames';
import Input from '../../../../Common/Input'

const Second_Step = () => {

    const [select, setSelect] = useState("");

    return (
        <Fragment>
            <div className={SecondCss.input1}>
               <Input
                    title="نام و نام خانوادگی"
                    type="text"
                    name="fullname"
                /> 
               
            </div>
        

            <div className={SecondCss.check_ti}>انتخاب روش ارسال:</div>
            <div className={cx(SecondCss.input1, SecondCss.i2)}>

                <div className={SecondCss.group1}>
           {/*          <input
                        className={SecondCss.inputcheck}
                        type="radio"
                        name="send"
                        checked={select === "tipax"}
                        value="tipax"
                        onClick={(e) => setSelect(e.target.value)}
                        htmlFor="send" />
                    <label className={SecondCss.lbl} >تیپاکس</label> */}

                </div>
                <div className={SecondCss.group1}>

                 {/*    <input
                        className={SecondCss.inputcheck}
                        value="post"
                        checked={select === "post"}
                        onClick={(e) => setSelect(e.target.value)}
                        type="radio" name="send" />
                    <label className={SecondCss.lbl} htmlFor="send" >پست</label> */}

                </div>

            </div>
        </Fragment>
    );
}

export default Second_Step;
