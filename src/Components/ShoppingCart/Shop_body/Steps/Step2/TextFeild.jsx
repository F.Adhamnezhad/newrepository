import React from 'react';
import { ErrorMessage, useField } from 'formik';
import Input from '../../../../Common/Input';
import { Fragment } from 'react';

const TextFeild = ({ name ,...props }) => {
    const [field, meta] = useField(props);
    return (
        <Fragment>
            <Input
                {...field} {...props}
            />
           
           <ErrorMessage component="div" name={field.name} /> 
        </Fragment>
    );
}

export default TextFeild;
