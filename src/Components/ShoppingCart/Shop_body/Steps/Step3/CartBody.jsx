import React, { Fragment } from 'react';
import ThirdCss from './Third_step.module.css';
import CustomInput from '../../../../Common/CustomInput';
import DataPicker from './DatePicker'

const CartBody = () => {

    return (
        <Fragment>
            <div className={ThirdCss.input}>
                <CustomInput spanCss={ThirdCss.spanCss} name="cartNumber" type="text" title="شماره کارت" />
                <DataPicker name="paymentDate" type="date"  title="تاریخ واریز" />
            </div>
        </Fragment>
    );
}

export default CartBody;
