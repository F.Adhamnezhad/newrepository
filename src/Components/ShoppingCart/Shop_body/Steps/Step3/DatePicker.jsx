import React from 'react';
import { Fragment } from 'react';
import { useField, Field , useFormikContext } from 'formik';
import { DatePicker } from "jalali-react-datepicker";
import DatePickerCss from './DatePicker.module.css';

const Date = ({title,...props }) => {
    const [field, meta] = useField(props);
    console.log("name", field.name.value)
    return (
        <Fragment>
            <div className={`col-12 col-md-6 ${DatePickerCss.main}`}>
                <span>{title}</span>
                <DatePicker
                 className={DatePickerCss.input}
                 />
            </div>

        </Fragment>
    );
}

export default Date;
