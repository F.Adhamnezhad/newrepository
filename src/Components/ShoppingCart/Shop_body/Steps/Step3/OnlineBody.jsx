import React, { Fragment, useState } from 'react';
import ThirdCss from './Third_step.module.css';
import cx from 'classnames';
import Input from '../../../../Common/PaymentInput';
import { Formik, Form, } from 'formik';
import ValidateForm from '../../../../../Core/Validations/ShoppingForm';
const OnlineBody = (props) => {

    return (
        <Fragment>

            <div className={ThirdCss.divasli}>
                <div className={ThirdCss.group}>
                    <label className={ThirdCss.lbl} >پرداخت از طریق ملت
                        <Input
                            autoComplete="off"
                            className={ThirdCss.inputcheck} value="1" type="radio" name="payment" />
                    </label>

                </div>
                <div className={ThirdCss.group}>
                    <label className={cx(ThirdCss.lbl2, ThirdCss.lbl)}>پرداخت از طریق آسان پرداخت
                        <Input
                            autoComplete="off"
                            className={ThirdCss.inputcheck} value="2" type="radio" name="payment" /></label>

                </div>
            </div>

        </Fragment>
    );
}

export default OnlineBody;
