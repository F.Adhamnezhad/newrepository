import React, { Fragment } from 'react';
import ThirdCss from './Third_step.module.css'

const OnlineHead = (props) => {
    return (
        <Fragment>
            <div className={ThirdCss.head}>
                <div className={ThirdCss.div}> </div>
                <span className={ThirdCss.span}>{props.title}</span>
            </div>
        </Fragment>
    );
}

export default OnlineHead;
