import React, { Fragment, useState, useContext, useEffect } from 'react';
import PriceBox from '../../PriceBox';
import { Formik, Form } from 'formik';
import Nav from '../Nav';
import Accordion_Component from '../../../../ContactUs/Content/FAQ/Accordion_Component';
import ThirdCss from './Third_step.module.css';
import OnlineHead from './OnlineHead';
import OnlineBody from './OnlineBody';
import CartBody from './CartBody';
import ValidateForm from '../../../../../Core/Validations/ShoppingForm'
import axios from 'axios';
import { CartContext } from '../../../CartContext';

const Payment = (props) => {
    const [accordionData, setAccordionData] = useState([
        { id: 0, header: <OnlineHead title="پرداخت اینترنتی" />, body: <OnlineBody next={props.next} Data={props.Data} /> },
        /* { id: 1, header: <OnlineHead title="کارت به کارت" />, body: <CartBody /> } */]);

    const [cart, setCart] = useContext(CartContext);
    const ShopProducts = cart.map(c => `${c.id}:${c.priceId}:${c.qty}`);
    const AllData = props.Data;
    const API_URL = props.url;
    const doSubmit = (values) => {
        props.next(values);
        var FormData = require('form-data');
        var data = new FormData();
        data.append('products', ShopProducts);
        data.append('full_name', values.fullname.toString());
        data.append('address', values.address.toString());
        data.append('mobile', values.phonenumber.toString());
        data.append('payment_type', 'internet');
        data.append('account_number', '5047061047534470');
        data.append('paid_date', '1399,03,14 14:20');
        data.append('post_id', values.code.toString());
        data.append('payment_gateway', '3');
        data.append('post_type', '1');

        var config = {
            method: 'post',
            url: `${API_URL}/order/create`,
            data: data
        };
        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    console.log("dataaaacarrrttt", cart)
    return (
        <Fragment>
            <Formik
                initialValues={AllData}
                validationSchema={ValidateForm}
                onSubmit={(values) => doSubmit(values)}
                enableReinitialize="true"
            >
                {() => (
                    <Form>
                        <div className={`col-12 ${ThirdCss.main}`}>
                            <div className={`col-md-8 col-12 ${ThirdCss.Right}`}>
                                <Nav status={props.status} />
                                <Accordion_Component data={accordionData} />
                            </div>
                            <div className={`col-md-4 col-12 ${ThirdCss.Left}`}>
                                <PriceBox prevColor="#eee" color="#f39237" btnPrev="بازگشت" handleBack={props.handleBack} btnType="submit" btnTitle="بررسی و پرداخت" />
                            </div>
                            <div className={ThirdCss.stick}>
                                <span>
                                    قیمت کل : {cart.reduce((acc, curr) => acc + curr.qty * curr.price, 0) - cart.reduce((acc, curr) => acc + curr.qty * curr.discount, 0)}
                                </span>
                                <button onClick={props.handleStateFunc}>
                                    تایید و ادامه
                                </button>
                                <button className={ThirdCss.backbtn} onClick={props.handleBack}>
                                </button>
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        </Fragment>
    );
}

export default Payment;
