import React, { Fragment , useState } from 'react';
import { Accordion, CardBody, Card } from 'react-bootstrap';
import Accordion_Component from '../../../../ContactUs/Content/FAQ/Accordion_Component';
import ThirdCss from './Third_step.module.css';
import OnlineHead from './OnlineHead';
import OnlineBody from './OnlineBody';
import CartBody from './CartBody';

const Third_step = (props) => {
    const [accordionData, setAccordionData] = useState( [
        { id: 0, header: <OnlineHead title="پرداخت اینترنتی"/> , body: <OnlineBody next={props.next} Data={props.Data}/> },
        { id: 1, header: <OnlineHead title="کارت به کارت"/> , body: <CartBody/> }]);
    return (
        <Fragment>
            <Accordion_Component data={accordionData} />
        </Fragment>
    );
}

export default Third_step;

