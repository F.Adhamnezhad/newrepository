import React, { Fragment } from 'react';
import Forth_Step from './Forth_Step';
import ForthCss from './Forth_Step.module.css';
import Nav from '../Nav'
import PriceBox from '../../PriceBox';
import pic from '../../../../../Assets/img/Shopping/Success payment.svg';
import {Link} from 'react-router-dom'

const FinalStep = (props) => {
    return (
        <Fragment>
            <div className={`col-12 ${ForthCss.main}`}>
                <div className={`col-md-8 col-12 ${ForthCss.Right}`}>
                    <Nav status={props.status} />
                    <Forth_Step pic={pic} message="پرداخت موفق" description="عملیات با موفقیت انجام شد"/>
                </div>
                <div className={`col-md-4 col-12 ${ForthCss.Left}`}>
                    <PriceBox color="#35F391" handleStateFunc={props.handleStateFunc} btnTitle="پیگیری سفارش" />
                </div>
            </div>      
        </Fragment>
    );
}

export default FinalStep;
