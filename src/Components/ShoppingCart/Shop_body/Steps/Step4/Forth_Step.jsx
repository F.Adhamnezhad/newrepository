import React, { useState, Fragment } from 'react';
import ForthCss from './Forth_Step.module.css';

const Forth_Step = (props) => {
    const [number, setnumber] = useState("12536");
    return (
        <Fragment>
            <div className={ForthCss.divmain}>
                <figure>
                    <img src={props.pic} />
                </figure>
                <h4 className={ForthCss.h4}>{props.message}</h4>
                <span className={ForthCss.desc}>{props.description}</span>
                <div className={ForthCss.numb}>
                    <span>شماره پیگیری:</span>
                    <span>{number}</span>
                </div>
            </div>
        </Fragment>
    );
}

export default Forth_Step;
