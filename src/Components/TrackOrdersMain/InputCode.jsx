import React from 'react';
import { ErrorMessage, useField } from 'formik';
import TrackCss from './Track.module.css';
import cx from 'classnames';

const InputCode = ({...props}) => {
    const [field, meta] = useField(props);
    return (
        <div className="container-fluid">
            <input
                className={ meta.touched && meta.error ? `col-11 col-md-8 ${cx(TrackCss.input ,TrackCss.inputErr )}` : `col-11 col-md-8 ${TrackCss.input}` }
                {...field} {...props}
                autoComplete="off"
            />
            <div>
                <ErrorMessage component="div" className={`col-11 col-md-8  ${TrackCss.err}`} name={field.name} />
            </div>
        </div>
    );
}

export default InputCode;
