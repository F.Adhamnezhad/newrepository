import React, { Fragment } from 'react';
import OrderCss from './OrderList.module.css';

const OrderList = (props) => {
    return (
        <Fragment>
            <div className={OrderCss.main}>
                <span className={OrderCss.productName}> {props.productCount>1 ? props.productName + ' * ' + props.productCount : props.productName}</span>
                <span className={OrderCss.productDash}>  - - - - - - - - - - - - - - - -  </span>
                <span className={OrderCss.productPrice}> {props.productPrice} ریال </span>
            </div>
        </Fragment>
    );
}

export default OrderList;
