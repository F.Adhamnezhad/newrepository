import React, { Fragment } from 'react';
import StatusCss from  './StatusSec.module.css';
import cx from 'classnames';

const StatusSec = (props) => {
    return (
        <Fragment>
            <div className={StatusCss.main}>
                <div className={props.StatusId === 0 && cx(StatusCss.right,StatusCss.first) || props.StatusId === 1 && cx(StatusCss.right,StatusCss.second) || props.StatusId === 2 && cx(StatusCss.right,StatusCss.third)}>
                    <span className={StatusCss.title}>{props.StatusTitle}</span>
                    <div  className={StatusCss.info}>
                    <span className={StatusCss.date}>{props.StatusDate}</span>
                    <span className={StatusCss.time}>ساعت {props.StatusTime}</span>
                    </div>
                </div>
                <div className={props.Status ? StatusCss.true : cx(StatusCss.false,StatusCss.true) }>

                </div>
            </div>
        </Fragment>
    );
}

export default StatusSec;
