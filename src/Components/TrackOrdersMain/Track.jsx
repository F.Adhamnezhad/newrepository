import React, { Fragment, useState, useEffect } from 'react';
import TrackCss from './Track.module.css';
import RouteHeader from '../Common/RouteHeader';
import StatusSec from './StatusSec';
import { Formik, Form, } from 'formik';
import OrderList from './OrderList';
import FaqQuestions from '../ContactUs/Content/FAQ/FaqQuestions';
import Axios from 'axios';
import TrackOrderValidation from '../../Core/Validations/TrackOrderValidation';
import InputCode from './InputCode';
import Accordion_Component from '../ContactUs/Content/FAQ/Accordion_Component';

const dataFAQ = [
    { id: 0, header: "چند روز طول میکشه به دستم برسه؟", body: "پاسخ سوالات", open: true },
    { id: 1, header: "چند روز طول میکشه به دستم برسه؟", body: "پاسخ سوالات", open: true },
    { id: 2, header: "چند روز طول میکشه به دستم برسه؟", body: "پاسخ سوالات", open: false },
    { id: 3, header: "چند روز طول میکشه به دستم برسه؟", body: "پاسخ سوالات", open: false },
    { id: 4, header: "چند روز طول میکشه به دستم برسه؟", body: "پاسخ سوالات", open: false }
];

const StatusData = [
    { id: 0, title: "بسته بندی شده", date: "1400/01/04", time: "10:11", status: true },
    { id: 1, title: "تحویل تیپاکس", date: "1400/01/04", time: "10:11", status: true },
    { id: 2, title: "در مسیر مقصد", date: "1400/01/04", time: "10:11", status: false }
]
const ProductsData = [
    { id: 0, name: "چای بهاره لاهیجان", price: "120000", count: "2" },
    { id: 1, name: "زنبیل دستباف", price: "700000", count: "1" },
    { id: 2, name: "چای بهاره لاهیجان", price: "120000", count: "2" },
    { id: 3, name: "زنبیل دستباف", price: "700000", count: "3" },
    { id: 4, name: "هزینه ارسال تیپاکس", price: "120000" }
]
const Track = (props) => {

    const [TrackOrderData, setTrackOrderData] = useState({ id:"" });
    const [result, setResult] = useState(true);
    const [Trackdata, setTrackdata] = useState([]);
    const API_URL = props.url;
    const handleSubmit = async (newData) => {
        setTrackOrderData((prev) => ({ ...prev, ...newData }));
        var FormData = require('form-data');
        var data1 = new FormData();
        data1.append('id', newData.id);
        var config = {
            method: 'post',
            url: 'http://api.jora.market/order/tracking',
            data: data1
        };

        Axios(config)
            .then(function (response) {
                (response.data.error) ? setResult(response.data) : setResult(false)
                console.log(JSON.stringify(response.data));
                setTrackdata(response.data)
            })

            .catch(function (error) {
                console.log(error);
            });
    }

    console.log("trackingData", Trackdata);
    const [status, setStatus] = useState(StatusData);
    const [data, setData] = useState(dataFAQ);
    console.log("productTrack", Trackdata.products);
    return (
        <Fragment>
            <div className={`container-fluid ${TrackCss.base}`} >
                <div className={`col-12 ${TrackCss.top}`}>
                    <RouteHeader pageTitle="پیگیری سفارشات" />
                    {
                        result &&
                        <div className={TrackCss.message}>
                            {result.message}
                        </div>
                    }

                    <Formik
                        initialValues={TrackOrderData}
                        validationSchema={TrackOrderValidation}
                        onSubmit={(newData) => { handleSubmit(newData) }}
                        enableReinitialize="true"
                    >
                        {() => (
                            <Form className={TrackCss.form}>
                                <InputCode
                                    name="id"
                                    type="number"
                                    placeholder="لطفا کد پیگیری سفارش را وارد نمایید"
                                />
                                <button className={TrackCss.btn}>پیگیری سفارش</button>
                            </Form>
                        )}
                    </Formik>
                </div>

                {!result ?
                    <div className="container-fluid">
                        <div className={`col-12 ${TrackCss.content}`}>
                            <div className={`col-12 col-lg-3 ${TrackCss.status}`}>
                                <span className={TrackCss.span}>وضعیت سفارش</span>
                                {status.map(i => (
                                    <StatusSec key={i.id} StatusTime={i.time} StatusId={i.id} StatusTitle={i.title} StatusDate={i.date} Status={i.status} />
                                ))}
                                <div className={TrackCss.dash1}></div>
                                <div className={TrackCss.dash}></div>
                            </div>
                            <div className={`col-12 col-lg-3 ${TrackCss.detail}`}>
                                <div className={TrackCss.main}>
                                    <div className={TrackCss.head}>
                                        <span className={TrackCss.span}>جزئیات سفارش</span>
                                        <span className={Trackdata.status === "ارسال شده" ? TrackCss.b : TrackCss.e}>{Trackdata.status}</span>
                                    </div>
                                    <div className={TrackCss.orderList}>
                                        <span className={TrackCss.ti}>لیست سفارش:</span>
                                       {Trackdata.products &&
                                       
                                       Trackdata.products.map(i =>
                                            <OrderList key={i.name} productName={i.name} productPrice={i.amount} productCount={i.count} />
                                        )} 

                                        {Trackdata.discount &&
                                            <div className={TrackCss.total}>
                                                <>
                                                    <div>
                                                        <span className={TrackCss.t} >تخفیف</span>
                                                        <span className={TrackCss.t}>{Trackdata.discount} ریال</span>
                                                    </div>
                                                    <div>
                                                        <span>هزینه کل</span>
                                                        <span>{Trackdata.amount} ریال</span>
                                                    </div>
                                                </>
                                            </div>
                                        }



                                    </div>

                                    <button className={TrackCss.btn_resid}>دانلود رسید</button>
                                </div>

                            </div>
                            <div className={`col-12 col-lg-3 ${TrackCss.faq}`}>

                                <div className={`col-12 ${TrackCss.right}`}>
                                    <span className={TrackCss.faq_ti}> سوالات متداول</span>
                                    <div className={TrackCss.data} >
                                        <FaqQuestions data={data} />
                                    </div>
                                </div>

                            </div>
                            <div className={`col-2 ${TrackCss.left}`}></div>
                        </div>
                    </div>
                    :
                    <div className={`col-12 col-md-10 ${TrackCss.div_faq}`}>
                        <span>سوالات متداول</span>
                        <Accordion_Component data={dataFAQ} />
                    </div>
                }
            </div>
        </Fragment>
    );
}

export default Track;
