import * as Yup from 'yup';

const ValidateContactForm = Yup.object().shape({
    userName: Yup.string().required("نام و نام خانوادگی را وارد نمایید"),
    phoneNumber:  Yup.number().typeError('شماره تلفن باید عدد باشد').min(11 , "لطفا مقدار صحیح وارد نمایید").required("شماره تلفن همراه را وارد نمایید "),
    email: Yup.string().matches(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ , "لطفا مقدار صحیح را وارد نمایید"),
    message:Yup.string().required("پیغام خود را وارد نمایید"),

})
export default ValidateContactForm;
