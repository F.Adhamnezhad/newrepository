import * as Yup from 'yup';

const ValidateForm = Yup.object().shape({
    fullname: Yup.string().required("نام و نام خانوادگی را وارد نمایید"),
    address: Yup.string().required(" آدرس را وارد نمایید"),
    code: Yup.number().typeError(' کد پستی باید عدد باشد').min(10 , "لطفا مقدار صحیح وارد نمایید").required('کد پستی را وارد نمایید'),
    phonenumber: Yup.number().typeError('شماره تلفن باید عدد باشد').min(11 , "لطفا مقدار صحیح وارد نمایید").required("شماره تلفن همراه را وارد نمایید "),
    cartNumber:Yup.number().typeError('شماره کارت باید عدد باشد')
})
export default ValidateForm;
