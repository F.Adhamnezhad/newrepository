import * as Yup from 'yup';

const TrackOrderValidation = Yup.object().shape({
    id: Yup.string().matches(/^10000*/,"لطفا مقدار صحیح را وارد نمایید").typeError('کد پیگیری باید عدد باشد').required("لطفا کد پیگیری را وارد نمایید")
});
export default TrackOrderValidation;
