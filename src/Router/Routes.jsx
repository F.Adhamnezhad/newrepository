import React, { Fragment } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Header from '../Components/Header/Header';
import Footer from '../Components/Footer/Footer';
import ContactUs from '../Screens/ContactUs';
import Landing from '../Screens/Landing';
import ProductDetails from '../Screens/ProductDetails';
import Products from '../Screens/Products';
import ShoppingCart from '../Screens/ShoppingCart';
import TrackOrders from '../Screens/TrackOrders';
import NotFound from '../Screens/NotFound';
import Pagination from '../Components/Products/PaginationList';
import FAQ from '../Components/ContactUs/Content/FAQ/FAQ'
import TrackOrderLanding from '../Components/TrackOrdersMain/TrackOrderLanding';
const Routes = (props) => {
    return (
        <Router>
            <Fragment>
                <Header getCartTotal={props.getCartTotal} />
                <Switch>
                    <Route exact component={() => <Landing  url={props.url} />} path="/" />
                    <Route exact component={() => <Products url={props.url} />} path="/products" />
                    <Route exact component={() => <ProductDetails url={props.url} />} path="/productDetail/:productId" />
                    <Route exact component={() => <ShoppingCart url={props.url} />} path="/shoppingcart" />
                    <Route exact component={() => <TrackOrders value={props.value} setValue={props.setValue} url={props.url} />} path="/trackorders" />
                    <Route exact component={ContactUs} path="/contactus" />
                    <Route component={() => <ContactUs selectId={1} />} path="/contactus/contact" />
                    <Route component={() => <ContactUs selectId={2} />} path="/contactus/about" />
                    <Route component={() => <ContactUs selectId={3} />} path="/contactus/faq" />
                    <Route exact component={NotFound} path="/404" />
                    <Route exact component={() => <TrackOrderLanding value={props.value} setValue={props.setValue} url={props.url} />} path="/trackordersLanding" />
                    <Route exact component={() => <TrackOrders url={props.url} />} path="/trackorders/:trackId" />
                    <Redirect to="/404" />
                </Switch>
                <Footer />
            </Fragment>
        </Router>
    );
}

export default Routes;
