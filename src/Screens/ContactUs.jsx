import React, { Fragment } from 'react';
import ContactTab from '../Components/ContactUs/ContactTab';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';



const ContactUs = (props) => {
    return (
        <Fragment>
            <div style={{ border:'1px solid #e5e5e531', background:"#e5e5e531"}}>
            <ContactTab selectId={props.selectId || 1}/>
            </div>
        </Fragment>
    );
}

export default ContactUs;
