import React, { Fragment,useState } from 'react';
import Header from "../Components/Header/Header";
import Box from "../Components/Landing/Box";
import Footer from "../Components/Footer/Footer";
import Articles from "../Components/Landing/Articles/Articles";
import FirstSection from '../Components/Landing/FirstSection';
import Product from '../Components/Landing/Products/Product';
import Comments from '../Components/Landing/Comments/Comments';
import TrackOrder from '../Components/Landing/TrackOrders/TrackOrder';

const Landing = (props) => {
    
    return (
        <Fragment>
           <FirstSection />
           <Box />
           <Articles/>
           <Product url={props.url} />
           <Comments url={props.url} />
           <TrackOrder/>
        </Fragment>
    );
}

export default Landing;
