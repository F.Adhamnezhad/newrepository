import React, { Fragment,useEffect,useState } from 'react';
import Details from '../Components/ProductDetails/Details';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import Axios from 'axios';
const ProductDetails = (props) => {
     
    return (
        <Fragment>
            <div style={{ border:'1px solid #e5e5e531', background:"#e5e5e531"}}>
            <Details url={props.url} />
            </div>
        </Fragment>
    );
}

export default ProductDetails;
