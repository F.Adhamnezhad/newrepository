import React, { Fragment } from 'react';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import ProductsList from '../Components/Products/ProductsList';


const Products = (props) => {
    return (
        <Fragment>
            <div style={{ border:'1px solid #e5e5e531', background:"#e5e5e531"}}>
                <ProductsList url={props.url}/>
            </div>
        </Fragment>
    );
}

export default Products;
