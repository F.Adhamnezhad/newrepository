import React, { Fragment } from 'react';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import Final from '../Components/ShoppingCart/Final';


const ShoppingCart = (props) => {
    return (
        <Fragment>
            <div  style={{ border:'1px solid #e5e5e531', background:"#e5e5e531"}}>
                <Final url={props.url} cart={props.cart} />
            </div>
        </Fragment>
    );
}

export default ShoppingCart;
