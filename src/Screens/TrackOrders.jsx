import React, { Fragment } from 'react';
import Header from '../Components/Header/Header';
import Footer from '../Components/Footer/Footer'
import Track from '../Components/TrackOrdersMain/Track';
const TrackOrders = (props) => {
    return (
        <Fragment>
            <div style={{ border: '1px solid #e5e5e531', background: "#e5e5e531" }}>
                <Track value={props.value} setValue={props.setValue} url={props.url}/>
            </div>

        </Fragment>
    );
}

export default TrackOrders;
